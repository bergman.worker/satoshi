using PlayFab.MultiplayerModels;
using PlayFab.Samples;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CBS.Models;
using System;

namespace CBS
{
    public class MatchmakingModule : BaseAzureModule
    {
        private static string gameAllKey = "game_all";
        private static string gameWinsKey = "game_wins";
        private static string gameLosesKey = "game_loses";
        private static string gameWinrateKey = "game_winrate";

        [FunctionName(AzureFunctions.CreatePVPMethod)]
        public static async Task<dynamic> CreatePVPMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionCreatePVPRequest>();
            var ProfileID = request.ProfileID;
            var OpponentId = request.OpponentId;
            var bet = request.bet;
            var code = "SC";

            var getProfileCurrencyResult = await CurrencyModule.GetProfileCurrenciesAsync(ProfileID);
            if (getProfileCurrencyResult.Error != null)
            {
                return ErrorHandler.ThrowError(getProfileCurrencyResult.Error).AsFunctionResult();
            }

            if (!CurrencyModule.EnoughFundsToSubtract(getProfileCurrencyResult.Result, code, bet))
            {
                return ErrorHandler.ThrowError(CBSError.FromMessage("You do not have enough funds in your account.")).AsFunctionResult();
            }

            var constraints = new CBSProfileConstraints 
            {
                LoadOnlineStatus = true,
            };

            var profileResult = await TableProfileAssistant.GetProfileDetailAsync(OpponentId, constraints);
            if (profileResult.Error != null)
            {
                return ErrorHandler.ThrowError(profileResult.Error).AsFunctionResult();
            }
            var profileDetail = profileResult.Result;

            if (!profileDetail.OnlineStatus.IsOnline) {
                return ErrorHandler.ThrowError(CBSError.FromMessage("Player is offline.")).AsFunctionResult();
            }

            var getOpponentCurrencyResult = await CurrencyModule.GetProfileCurrenciesAsync(OpponentId);
            if (getOpponentCurrencyResult.Error != null)
            {
                return ErrorHandler.ThrowError(getOpponentCurrencyResult.Error).AsFunctionResult();
            }

            if (!CurrencyModule.EnoughFundsToSubtract(getOpponentCurrencyResult.Result, code, bet))
            {
                return ErrorHandler.ThrowError(CBSError.FromMessage("The player does not have enough funds on the account.")).AsFunctionResult();
            }

            var getResult =  await TablePVPAssistant.CreatePVP(ProfileID, OpponentId, bet);
            if (getResult.Error != null)
            {
                return ErrorHandler.ThrowError(getResult.Error).AsFunctionResult();
            }

            var Result = new FunctionCheckPVPResult
                {
                    pvpId = getResult.Result.pvp.Id,
                    CreatorId = getResult.Result.pvp.CreatorId,
                    OpponentId = getResult.Result.pvp.OpponentId,
                    CreatorName = "",
                    OpponentName = "",
                    bet = getResult.Result.pvp.bet,
                    Status = getResult.Result.pvp.Status
                };

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.CheckPVPMethod)]
        public static async Task<dynamic> CheckPVPMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionBaseRequest>();
            var ProfileID = request.ProfileID;

            var pvpResult = await TablePVPAssistant.GetPVPList(ProfileID);

            if (pvpResult.Error != null)
            {
                return ErrorHandler.ThrowError(pvpResult.Error).AsFunctionResult();
            }

            var constraints = new CBSProfileConstraints();
            var creatorProfileResult = await TableProfileAssistant.GetProfileDetailAsync(pvpResult.Result.pvp.CreatorId, constraints);
            if (creatorProfileResult.Error != null)
            {
                return ErrorHandler.ThrowError(creatorProfileResult.Error).AsFunctionResult();
            }

            var creatorName = creatorProfileResult.Result.DisplayName;

            var opponentProfileResult = await TableProfileAssistant.GetProfileDetailAsync(pvpResult.Result.pvp.OpponentId, constraints);
            if (opponentProfileResult.Error != null)
            {
                return ErrorHandler.ThrowError(opponentProfileResult.Error).AsFunctionResult();
            }

            var opponentName = opponentProfileResult.Result.DisplayName;

            var Result = new FunctionCheckPVPResult
                {
                    pvpId = pvpResult.Result.pvp.Id,
                    CreatorId = pvpResult.Result.pvp.CreatorId,
                    OpponentId = pvpResult.Result.pvp.OpponentId,
                    CreatorName = creatorName,
                    OpponentName = opponentName,
                    bet = pvpResult.Result.pvp.bet,
                    Status = pvpResult.Result.pvp.Status
                };

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.AcceptPVPMethod)]
        public static async Task<dynamic> AcceptPVPMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionAcceptPVPRequest>();
            var pvpId = request.pvpId;

            var pvpResult = await TablePVPAssistant.AcceptPVP(pvpId);

            if (pvpResult.Error != null)
            {
                return ErrorHandler.ThrowError(pvpResult.Error).AsFunctionResult();
            }

            //await CurrencyModule.SubtractVirtualCurrencyFromProfileAsync(pvpResult.Result.pvp.CreatorId, "SC", pvpResult.Result.pvp.bet);
            //await CurrencyModule.SubtractVirtualCurrencyFromProfileAsync(pvpResult.Result.pvp.OpponentId, "SC", pvpResult.Result.pvp.bet);

            return pvpResult.Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.DeclinePVPMethod)]
        public static async Task<dynamic> DeclinePVPMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionAcceptPVPRequest>();
            var pvpId = request.pvpId;

            var pvpResult = await TablePVPAssistant.DeclinePVP(pvpId);

            if (pvpResult.Error != null)
            {
                return ErrorHandler.ThrowError(pvpResult.Error).AsFunctionResult();
            }

            return pvpResult.Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.CancelPVPMethod)]
        public static async Task<dynamic> CancelPVPMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionAcceptPVPRequest>();
            var pvpId = request.pvpId;

            var pvpResult = await TablePVPAssistant.CancelPVP(pvpId);

            if (pvpResult.Error != null)
            {
                return ErrorHandler.ThrowError(pvpResult.Error).AsFunctionResult();
            }

            return pvpResult.Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.StartPVPMethod)]
        public static async Task<dynamic> StartPVPMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionAcceptPVPRequest>();
            var pvpId = request.pvpId;

            var pvpResult = await TablePVPAssistant.StartPVP(pvpId);

            if (pvpResult.Error != null)
            {
                return ErrorHandler.ThrowError(pvpResult.Error).AsFunctionResult();
            }

            await CurrencyModule.SubtractVirtualCurrencyFromProfileAsync(pvpResult.Result.pvp.CreatorId, "SC", pvpResult.Result.pvp.bet);
            await CurrencyModule.SubtractVirtualCurrencyFromProfileAsync(pvpResult.Result.pvp.OpponentId, "SC", pvpResult.Result.pvp.bet);

            return pvpResult.Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.FinishPVPMethod)]
        public static async Task<dynamic> FinishPVPMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionFinishPVPRequest>();
            var pvpId = request.pvpId;
            var winnerId = request.winnerId;
            var codeSC = "SC";

            var getPVPResult = await TablePVPAssistant.GetPVP(pvpId);
            if (getPVPResult.Error != null)
            {
                return ErrorHandler.ThrowError(getPVPResult.Error).AsFunctionResult();
            }

            if (getPVPResult.Result.pvp.Status != "FINISHED") {
                await TablePVPAssistant.FinishPVP(getPVPResult.Result.pvp.entity);
                await CurrencyModule.AddVirtualCurrencyToProfileAsync(winnerId, codeSC, (int) (getPVPResult.Result.pvp.bet * 1.9));
            }
            var Result = new FunctionEmptyResult();

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.StartVersusBattleMethod)]
        public static async Task<dynamic> StartVersusBattleMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionStartVersusBattleRequest>();
            var ProfileID = request.ProfileID;
            var bet = request.bet;
            var codeSC = "SC";
            var codeEN = "EN";

            var energyForSubstract = 1;

            if (bet > 0) {
                energyForSubstract = 4;
            }

            var getProfileCurrencyResult = await CurrencyModule.GetProfileCurrenciesAsync(ProfileID);
            if (getProfileCurrencyResult.Error != null)
            {
                return ErrorHandler.ThrowError(getProfileCurrencyResult.Error).AsFunctionResult();
            }

            if (!CurrencyModule.EnoughFundsToSubtract(getProfileCurrencyResult.Result, codeEN, energyForSubstract))
            {
                return ErrorHandler.ThrowError(CBSError.FromMessage("You do not have enough energy.")).AsFunctionResult();
            }

            if (!CurrencyModule.EnoughFundsToSubtract(getProfileCurrencyResult.Result, codeSC, bet))
            {
                return ErrorHandler.ThrowError(CBSError.FromMessage("You do not have enough funds in your account.")).AsFunctionResult();
            }

            await CurrencyModule.SubtractVirtualCurrencyFromProfileAsync(ProfileID, codeEN, energyForSubstract);
            await CurrencyModule.SubtractVirtualCurrencyFromProfileAsync(ProfileID, codeSC, bet);

            var Result = new FunctionEmptyResult();

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.StartCasinoBattleMethod)]
        public static async Task<dynamic> StartCasinoBattleMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionStartVersusBattleRequest>();
            var ProfileID = request.ProfileID;
            var bet = request.bet;
            var codeSC = "SC";
            var codeEN = "EN";
            var energyBet = 2;

            var getProfileCurrencyResult = await CurrencyModule.GetProfileCurrenciesAsync(ProfileID);
            if (getProfileCurrencyResult.Error != null)
            {
                return ErrorHandler.ThrowError(getProfileCurrencyResult.Error).AsFunctionResult();
            }

            if (!CurrencyModule.EnoughFundsToSubtract(getProfileCurrencyResult.Result, codeEN, energyBet))
            {
                return ErrorHandler.ThrowError(CBSError.FromMessage("You do not have enough energy.")).AsFunctionResult();
            }

            if (!CurrencyModule.EnoughFundsToSubtract(getProfileCurrencyResult.Result, codeSC, bet))
            {
                return ErrorHandler.ThrowError(CBSError.FromMessage("You do not have enough funds in your account.")).AsFunctionResult();
            }

            await CurrencyModule.SubtractVirtualCurrencyFromProfileAsync(ProfileID, codeEN, energyBet);
            await CurrencyModule.SubtractVirtualCurrencyFromProfileAsync(ProfileID, codeSC, bet);

            var Result = new FunctionEmptyResult();

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.GetRewardsFromCasinoBattleMethod)]
        public static async Task<dynamic> GetRewardsFromCasinoBattleMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionGetRewardFromVersusBattleRequest>();
            var profileID = request.ProfileID;
            var isWinner = request.isWinner;
            var bet = request.bet;
            var amount = 0;


            if (isWinner) {
                amount = (int) (bet * 1.7);
                await CurrencyModule.AddVirtualCurrencyToProfileAsync(profileID, "SC", amount);
            }

            var Result = new FunctionGetRewardFromVersusBattleResult
                {
                    amount = amount,
                    exp = 0
                };

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.GetMatchmakingQueueMethod)]
        public static async Task<dynamic> GetMatchmakingQueueTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionMatchmakingQueueRequest>();
            var queue = request.Queue;

            var getResult = await GetMatchmakingQueueAsync(queue);
            if (getResult.Error != null)
            {
                return ErrorHandler.ThrowError(getResult.Error).AsFunctionResult();
            }

            return getResult.Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.GetMatchmakingListMethod)]
        public static async Task<dynamic> GetMatchmakingListTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionMatchmakingQueueRequest>();

            var getResult = await GetMatchmakingListAsync();
            if (getResult.Error != null)
            {
                return ErrorHandler.ThrowError(getResult.Error).AsFunctionResult();
            }

            return getResult.Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.UpdateMatchmakingQueueMethod)]
        public static async Task<dynamic> UpdateMatchmakingQueueTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionMatchmakingQueueRequest>();
            var queue = request.Queue;

            var updateResult = await UpdateMatchmakingQueueAsync(queue);
            if (updateResult.Error != null)
            {
                return ErrorHandler.ThrowError(updateResult.Error).AsFunctionResult();
            }

            return updateResult.Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.RemoveMatchmakingQueueMethod)]
        public static async Task<dynamic> RemoveMatchmakingQueueTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionMatchmakingQueueRequest>();
            var queue = request.Queue;

            var removeResult = await RemoveMatchmakingQueueAsync(queue);
            if (removeResult.Error != null)
            {
                return ErrorHandler.ThrowError(removeResult.Error).AsFunctionResult();
            }

            return removeResult.Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.GetMatchMethod)]
        public static async Task<dynamic> GetMatchTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionMatchRequest>();
            var queue = request.Queue;
            var matchID = request.MatchID;

            var getResult = await GetMatchAsync(queue, matchID);
            if (getResult.Error != null)
            {
                return ErrorHandler.ThrowError(getResult.Error).AsFunctionResult();
            }

            return getResult.Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.GetRewardsFromVersusBattleMethod)]
        public static async Task<dynamic> GetRewardsFromVersusBattleMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionGetRewardFromVersusBattleRequest>();
            var profileID = request.ProfileID;
            var isWinner = request.isWinner;
            var bet = request.bet;
            var gameAll = "0";
            var gameWins = "0";
            var gameLoses = "0";
            var gameWinrate = "0";
            int amount = 0;
            int exp = 0;

            var profileDataKeys = new string[]
            {
                gameAllKey,
                gameWinsKey,
                gameLosesKey,
                gameWinrateKey
            };

            var getGameStatistics = await ProfileModule.GetProfileDataAsync(profileID, profileDataKeys);

            if (getGameStatistics.Error != null) {
                return ErrorHandler.ThrowError(getGameStatistics.Error).AsFunctionResult();
            }

            var gameStatisticsDictionary = getGameStatistics.Result.Data;

            try {
                gameAll = gameStatisticsDictionary[gameAllKey].Value;
                gameWins = gameStatisticsDictionary[gameWinsKey].Value;
                gameLoses = gameStatisticsDictionary[gameLosesKey].Value;
                gameWinrate = gameStatisticsDictionary[gameWinrateKey].Value;
            } catch (Exception e) {
            }

            var gameAllInt = Int32.Parse(gameAll);
            var gameWinsInt = Int32.Parse(gameWins);
            var gameLosesInt = Int32.Parse(gameLoses);
            var gameWinrateInt = Int32.Parse(gameWinrate);

            gameAllInt++;

            if (isWinner) {
                gameWinsInt++;
            } else {
                gameLosesInt++;
            }

            gameWinrateInt = gameWinsInt / gameAllInt * 100;

            await ProfileModule.SetProfileDataAsync(profileID, gameAllKey, gameAllInt.ToString());
            await ProfileModule.SetProfileDataAsync(profileID, gameWinsKey, gameWinsInt.ToString());
            await ProfileModule.SetProfileDataAsync(profileID, gameLosesKey, gameLosesInt.ToString());
            await ProfileModule.SetProfileDataAsync(profileID, gameWinrateKey, gameWinrateInt.ToString());

            if (isWinner) {
                if (bet == 0) {
                    Random randomAmount = new Random();
                    amount = randomAmount.Next(1, 3);
                } else {
                    amount = (int) (bet * 1.9);
                }
                
                await CurrencyModule.AddVirtualCurrencyToProfileAsync(profileID, "SC", amount);
                Random randomExp = new Random();
                exp = randomExp.Next(10, 20);
                await ProfileExpModule.AddExpirienceToPlayerAsync(profileID, exp);
                var clanProfileIDResult = await ProfileModule.GetProfileClanIDAsync(profileID);
                if (clanProfileIDResult.Error != null)
                {
                    return ErrorHandler.ThrowError(clanProfileIDResult.Error).AsFunctionResult();
                }
                var clanID = clanProfileIDResult.Result;
                await ClanExpModule.AddExpirienceToClanAsync(clanID, exp);
            }

            var Result = new FunctionGetRewardFromVersusBattleResult
                {
                    amount = amount,
                    exp = exp
                };

            return Result.AsFunctionResult();
        }

        public static async Task<ExecuteResult<GetMatchmakingQueueResult>> GetMatchmakingQueueAsync(string queueName)
        {
            var multiplayerApi = await GetFabMultiplayerAPIAsync();

            var request = new GetMatchmakingQueueRequest {
                QueueName = queueName
            };
            
            var getQueueResult = await multiplayerApi.GetMatchmakingQueueAsync(request);
            if (getQueueResult.Error != null)
            {
                return ErrorHandler.ThrowError<GetMatchmakingQueueResult>(getQueueResult.Error);
            }

            return new ExecuteResult<GetMatchmakingQueueResult>
            {
                Result = getQueueResult.Result
            };
        }

        public static async Task<ExecuteResult<ListMatchmakingQueuesResult>> GetMatchmakingListAsync()
        {
            var multiplayerApi = await GetFabMultiplayerAPIAsync();
            var request = new ListMatchmakingQueuesRequest();
	
            var listResult = await multiplayerApi.ListMatchmakingQueuesAsync(request);
            if (listResult.Error != null)
            {
                return ErrorHandler.ThrowError<ListMatchmakingQueuesResult>(listResult.Error);
            }

            return new ExecuteResult<ListMatchmakingQueuesResult>
            {
                Result = listResult.Result
            };
        }

        public static async Task<ExecuteResult<SetMatchmakingQueueResult>> UpdateMatchmakingQueueAsync(string queueRaw)
        {
            var multiplayerApi = await GetFabMultiplayerAPIAsync();

            var rawData = string.IsNullOrEmpty(queueRaw) ? JsonPlugin.EMPTY_JSON : queueRaw;
            var queue = JsonConvert.DeserializeObject<MatchmakingQueueConfig>(rawData);

            var request = new SetMatchmakingQueueRequest {
                MatchmakingQueue = queue
            };
            
            var setResult = await multiplayerApi.SetMatchmakingQueueAsync(request);
            if (setResult.Error != null)
            {
                return ErrorHandler.ThrowError<SetMatchmakingQueueResult>(setResult.Error);
            }

            return new ExecuteResult<SetMatchmakingQueueResult>
            {
                Result = setResult.Result
            };
        }

        public static async Task<ExecuteResult<RemoveMatchmakingQueueResult>> RemoveMatchmakingQueueAsync(string queueName)
        {
            var multiplayerApi = await GetFabMultiplayerAPIAsync();

            var request = new RemoveMatchmakingQueueRequest {
                QueueName = queueName
            };
            
            var removeResult = await multiplayerApi.RemoveMatchmakingQueueAsync(request);
            if (removeResult.Error != null)
            {
                return ErrorHandler.ThrowError<RemoveMatchmakingQueueResult>(removeResult.Error);
            }

            return new ExecuteResult<RemoveMatchmakingQueueResult>
            {
                Result = removeResult.Result
            };
        }

        public static async Task<ExecuteResult<GetMatchResult>> GetMatchAsync(string queueName, string matchID)
        {
            var multiplayerApi = await GetFabMultiplayerAPIAsync();

            var matchRequest = new GetMatchRequest
            {
                MatchId = matchID,
                QueueName = queueName,
                ReturnMemberAttributes = true
            };
            var getMatchResult = await multiplayerApi.GetMatchAsync(matchRequest);
            return new ExecuteResult<GetMatchResult>
            {
                Result = getMatchResult.Result
            };
        }
    }
}