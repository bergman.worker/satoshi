using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PlayFab.Samples;
using CBS.Models;
using PlayFab.ServerModels;
using Nethereum.Web3;
using Nethereum.Contracts.Standards.ERC20.ContractDefinition;
using Nethereum.Util;
using Nethereum.Web3.Accounts;
using Nethereum.KeyStore;
using Nethereum.HdWallet;
using NBitcoin;
using System.Numerics;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using System.Threading;
using Nethereum.Signer.EIP712;
using Nethereum.ABI.EIP712.EIP2612;
using Nethereum.Signer;
using System.Linq;
using Nethereum.Hex.HexTypes;

namespace CBS
{
    public class Web3Module : BaseAzureModule
    {

        private static string incomeTransactionHashKey = "i_transaction_hash";
        private static string incomeTransactionAmountKey = "i_transaction_amount";
        private static string incomeTransactionCurrencyKey = "i_transaction_currency";
        private static string incomeTransactionStatusKey = "i_transaction_status";
        private static string outcomeTransactionHashKey = "o_transaction_hash";
        private static string outcomeTransactionAmountKey = "o_transaction_amount";
        private static string outcomeTransactionCurrencyKey = "o_transaction_currency";
        private static string outcomeTransactionStatusKey = "o_transaction_status";

        private static string walletAddressKey = "wallet_address";
        private static string walletPrivateKeyKey = "wallet_private_key";

        private static string netUrl = "https://bsc-testnet.publicnode.com";
        private static int chainID = 97;
        private static string domainName = "TestSatoshiFigthCoin";
        private static string contractABI = "[{'inputs':[],'stateMutability':'nonpayable','type':'constructor'},{'inputs':[],'name':'InvalidShortString','type':'error'},{'inputs':[{'internalType':'string','name':'str','type':'string'}],'name':'StringTooLong','type':'error'},{'anonymous':false,'inputs':[{'indexed':true,'internalType':'address','name':'owner','type':'address'},{'indexed':true,'internalType':'address','name':'spender','type':'address'},{'indexed':false,'internalType':'uint256','name':'value','type':'uint256'}],'name':'Approval','type':'event'},{'anonymous':false,'inputs':[],'name':'EIP712DomainChanged','type':'event'},{'anonymous':false,'inputs':[{'indexed':true,'internalType':'address','name':'from','type':'address'},{'indexed':true,'internalType':'address','name':'to','type':'address'},{'indexed':false,'internalType':'uint256','name':'value','type':'uint256'}],'name':'Transfer','type':'event'},{'inputs':[],'name':'DOMAIN_SEPARATOR','outputs':[{'internalType':'bytes32','name':'','type':'bytes32'}],'stateMutability':'view','type':'function'},{'inputs':[{'internalType':'address','name':'owner','type':'address'},{'internalType':'address','name':'spender','type':'address'}],'name':'allowance','outputs':[{'internalType':'uint256','name':'','type':'uint256'}],'stateMutability':'view','type':'function'},{'inputs':[{'internalType':'address','name':'spender','type':'address'},{'internalType':'uint256','name':'amount','type':'uint256'}],'name':'approve','outputs':[{'internalType':'bool','name':'','type':'bool'}],'stateMutability':'nonpayable','type':'function'},{'inputs':[{'internalType':'address','name':'account','type':'address'}],'name':'balanceOf','outputs':[{'internalType':'uint256','name':'','type':'uint256'}],'stateMutability':'view','type':'function'},{'inputs':[],'name':'decimals','outputs':[{'internalType':'uint8','name':'','type':'uint8'}],'stateMutability':'view','type':'function'},{'inputs':[{'internalType':'address','name':'spender','type':'address'},{'internalType':'uint256','name':'subtractedValue','type':'uint256'}],'name':'decreaseAllowance','outputs':[{'internalType':'bool','name':'','type':'bool'}],'stateMutability':'nonpayable','type':'function'},{'inputs':[],'name':'eip712Domain','outputs':[{'internalType':'bytes1','name':'fields','type':'bytes1'},{'internalType':'string','name':'name','type':'string'},{'internalType':'string','name':'version','type':'string'},{'internalType':'uint256','name':'chainId','type':'uint256'},{'internalType':'address','name':'verifyingContract','type':'address'},{'internalType':'bytes32','name':'salt','type':'bytes32'},{'internalType':'uint256[]','name':'extensions','type':'uint256[]'}],'stateMutability':'view','type':'function'},{'inputs':[{'internalType':'address','name':'spender','type':'address'},{'internalType':'uint256','name':'addedValue','type':'uint256'}],'name':'increaseAllowance','outputs':[{'internalType':'bool','name':'','type':'bool'}],'stateMutability':'nonpayable','type':'function'},{'inputs':[],'name':'name','outputs':[{'internalType':'string','name':'','type':'string'}],'stateMutability':'view','type':'function'},{'inputs':[{'internalType':'address','name':'owner','type':'address'}],'name':'nonces','outputs':[{'internalType':'uint256','name':'','type':'uint256'}],'stateMutability':'view','type':'function'},{'inputs':[{'internalType':'address','name':'owner','type':'address'},{'internalType':'address','name':'spender','type':'address'},{'internalType':'uint256','name':'value','type':'uint256'},{'internalType':'uint256','name':'deadline','type':'uint256'},{'internalType':'uint8','name':'v','type':'uint8'},{'internalType':'bytes32','name':'r','type':'bytes32'},{'internalType':'bytes32','name':'s','type':'bytes32'}],'name':'permit','outputs':[],'stateMutability':'nonpayable','type':'function'},{'inputs':[],'name':'symbol','outputs':[{'internalType':'string','name':'','type':'string'}],'stateMutability':'view','type':'function'},{'inputs':[],'name':'totalSupply','outputs':[{'internalType':'uint256','name':'','type':'uint256'}],'stateMutability':'view','type':'function'},{'inputs':[{'internalType':'address','name':'to','type':'address'},{'internalType':'uint256','name':'amount','type':'uint256'}],'name':'transfer','outputs':[{'internalType':'bool','name':'','type':'bool'}],'stateMutability':'nonpayable','type':'function'},{'inputs':[{'internalType':'address','name':'from','type':'address'},{'internalType':'address','name':'to','type':'address'},{'internalType':'uint256','name':'amount','type':'uint256'}],'name':'transferFrom','outputs':[{'internalType':'bool','name':'','type':'bool'}],'stateMutability':'nonpayable','type':'function'}]";
        private static string contractAddress = "0xf5c46175c68bc480b1cfbef12ccbbb8387b6ff84";
        private static string bankPrivateKey = "0xa31294e9eee69da456582ce7c2022573a4ee08084540bed6376d18a8f70823e7";
        private static string bankAddress = "0xba03C449fBe11090Cf11a115ED6DCA32eBc09625";
        private static string ownerWalletPrivateKey = "0xa31294e9eee69da456582ce7c2022573a4ee08084540bed6376d18a8f70823e7";
        private static string ownerWalletAddress = "0xba03C449fBe11090Cf11a115ED6DCA32eBc09625";
        
        private static int gasPrice = 100;


        [FunctionName(AzureFunctions.GetMnemonicMethod)]
        public static async Task<dynamic> GetMnemonicMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
           Mnemonic mnemo = new Mnemonic(Wordlist.English, WordCount.Twelve);
            var Result = new FunctionMnemonicResult
                {
                    mnemonic = mnemo.ToString(),
                };

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.CreateWalletMethod)]
        public static async Task<dynamic> CreateWalletMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionMnemonicRequest>();
            var mnemonic = request.mnemonic;
            var profileID = request.ProfileID;

            Wallet wallet = new Nethereum.HdWallet.Wallet(mnemonic, "artempetuh");
            Account account = wallet.GetAccount(0);

            var address = account.Address;
            var privateKey = account.PrivateKey;

                await ProfileModule.SetProfileDataAsync(profileID, walletAddressKey, address);
                await ProfileModule.SetProfileDataAsync(profileID, walletPrivateKeyKey, privateKey);

            var Result = new FunctionWalletResult
                {
                    address = address,
                    privateKey = privateKey
                };

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.ImportWalletFromMnemonicMethod)]
        public static async Task<dynamic> ImportWalletFromMnemonicMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionMnemonicRequest>();
            var mnemonic = request.mnemonic;
            var profileID = request.ProfileID;

            Wallet wallet = new Nethereum.HdWallet.Wallet(mnemonic, "artempetuh");
            Account account = wallet.GetAccount(0);

            var address = account.Address;
            var privateKey = account.PrivateKey;

                await ProfileModule.SetProfileDataAsync(profileID, walletAddressKey, address);
                await ProfileModule.SetProfileDataAsync(profileID, walletPrivateKeyKey, privateKey);

            var Result = new FunctionWalletResult
                {
                    address = address,
                    privateKey = privateKey
                };

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.ImportWalletFromPrivateKeyMethod)]
        public static async Task<dynamic> ImportWalletFromPrivateKeyMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionPrivateKeyRequest>();
            var privateKey = request.privateKey;
            var profileID = request.ProfileID;

            var account = new Nethereum.Web3.Accounts.Account(privateKey);

            var address = account.Address;

                await ProfileModule.SetProfileDataAsync(profileID, walletAddressKey, address);
                await ProfileModule.SetProfileDataAsync(profileID, walletPrivateKeyKey, privateKey);

            var Result = new FunctionWalletResult
                {
                    address = address,
                    privateKey = privateKey
                };

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.GetWalletDataMethod)]
        public static async Task<dynamic> GetWalletDataMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionBaseRequest>();
            var profileID = request.ProfileID;
            string walletAddress, walletPrivateKey;
            var profileDataMap = new string[] 
            {
                walletAddressKey,
                walletPrivateKeyKey
            };
            var getWalletResult = await ProfileModule.GetProfileDataAsync(profileID, profileDataMap);

            if (getWalletResult.Error != null) {
                return ErrorHandler.ThrowError(getWalletResult.Error).AsFunctionResult();
            }

            var profileDictionary = getWalletResult.Result.Data;

            try {
                walletAddress = profileDictionary[walletAddressKey].Value;
                walletPrivateKey = profileDictionary[walletPrivateKeyKey].Value;
            } catch (Exception e) {
                return ErrorHandler.ThrowError(CBSError.FromMessage("No wallet found to display balance.")).AsFunctionResult();
            }

            if (walletAddress.Length == 0 || walletPrivateKey.Length == 0) {
                return ErrorHandler.ThrowError(CBSError.FromMessage("No wallet found to display balance.")).AsFunctionResult();
            }

            var account = new Nethereum.Web3.Accounts.Account(walletPrivateKey);
            var web3 = new Web3(account, netUrl);

            var balanceBNBWei = await web3.Eth.GetBalance.SendRequestAsync(account.Address);
            var balanceBNB = Web3.Convert.FromWei(balanceBNBWei.Value).ToString();

            var playerContract = web3.Eth.GetContract(contractABI, contractAddress);

            var functionBalanceOf = playerContract.GetFunction("balanceOf");
            var functionBalanceOfJson = @"{'account': '" + walletAddress + "'}";
            var functionBalanceOfValues = functionBalanceOf.ConvertJsonToObjectInputParameters(functionBalanceOfJson);
            var balanceSFCWei = await functionBalanceOf.CallAsync<BigInteger>(functionBalanceOfValues.ToArray());
            var balanceSFC = Web3.Convert.FromWei(balanceSFCWei).ToString();

            var getCurrencyResult = await CurrencyModule.GetProfileCurrenciesAsync(profileID);

            if (getCurrencyResult.Error != null)
            {
                return ErrorHandler.ThrowError(getCurrencyResult.Error).AsFunctionResult();
            }
            var currencies = getCurrencyResult.Result.Currencies;

            var gameCurrency = currencies["SC"];

            var Result = new FunctionWalletDataResult
                {
                    walletBNBBalance = balanceBNB,
                    walletTokenBalance = balanceSFC,
                    walletAddress = walletAddress,
                    gameBalance = gameCurrency.Value.ToString()
                };

            return Result.AsFunctionResult();
        }
       
        [FunctionName(AzureFunctions.PushToWalletMethod)]
        public static async Task<dynamic> PushToWalletMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, [DurableClient] IDurableOrchestrationClient starter, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionPushTokenRequest>();

            var profileID = request.ProfileID;
            var code = "SC";
            var amount = request.amount;
            string walletAddress;

         

            var profileDataMap = new string[] 
            {
                walletAddressKey,
                walletPrivateKeyKey,
                outcomeTransactionHashKey
            };

            var getWalletResult = await ProfileModule.GetProfileDataAsync(profileID, profileDataMap);

            if (getWalletResult.Error != null) {
                return ErrorHandler.ThrowError(getWalletResult.Error).AsFunctionResult();
            }

            var profileDictionary = getWalletResult.Result.Data;

            try {
                walletAddress = profileDictionary[walletAddressKey].Value;
            } catch (Exception e) {
                return ErrorHandler.ThrowError(CBSError.FromMessage("No withdrawal wallet found.")).AsFunctionResult();
            }

            if (walletAddress.Length == 0) {
                return ErrorHandler.ThrowError(CBSError.FromMessage("No withdrawal wallet found.")).AsFunctionResult();
            }

            try {
                var transactionHash = profileDictionary[outcomeTransactionHashKey].Value;
                if (transactionHash.Length != 0) {
                    return ErrorHandler.ThrowError(CBSError.FromMessage("Wait for the completion of the previous withdrawal request.")).AsFunctionResult();
                }
            } catch (Exception e) {}

            var getCurrencyResult = await CurrencyModule.GetProfileCurrenciesAsync(profileID);
            if (getCurrencyResult.Error != null)
            {
                return ErrorHandler.ThrowError(getCurrencyResult.Error).AsFunctionResult();
            }
            var currencies = getCurrencyResult.Result.Currencies;

            if (!CurrencyModule.EnoughFundsToSubtract(getCurrencyResult.Result, code, amount))
            {
                return ErrorHandler.ThrowError(CBSError.FromMessage("Insufficient funds to withdraw.")).AsFunctionResult();
            }

            await CurrencyModule.SubtractVirtualCurrencyFromProfileAsync(profileID, "SC", amount);
                
            string requestRaw = JsonPlugin.ToJsonCompress(request);
            var taskID = await starter.StartNewAsync(AzureFunctions.PushToWalletProcessMethod, null, requestRaw);

            var Result = new FunctionPushTokenResult
                {
                    Message = "Withdrawal request successfully created."
                };

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.PushToWalletProcessMethod)]
        public static async Task PushToWalletProcessMethodTrigger([OrchestrationTrigger] IDurableOrchestrationContext context)
        {
            var requestContent = context.GetInput<string>();
            DateTime waitAWhile = context.CurrentUtcDateTime.Add(TimeSpan.FromSeconds(10));
            await context.CreateTimer(waitAWhile, CancellationToken.None);
            await context.CallActivityAsync(AzureFunctions.PushToWalletPerformMethod, requestContent);
        }

        [FunctionName(AzureFunctions.PushToWalletPerformMethod)]
        public static async Task<string> PushToWalletPerformMethodTrigger([ActivityTrigger] string requestContent, ILogger log)
        {
            var request = JsonPlugin.FromJsonDecompress<FunctionPushTokenRequest>(requestContent);
            var profileId = request.ProfileID;
            var amount = request.amount;
            string walletPrivateKey;
            string walletAddress;

            var profileDataMap = new string[] 
            {
                walletPrivateKeyKey,
                walletAddressKey
            };

            var getWalletResult = await ProfileModule.GetProfileDataAsync(profileId, profileDataMap);

            if (getWalletResult.Error != null) {
                var notification = new CBSNotification{
                    ID = 1.ToString(),
                    Read = false,
                    Title = "Transaction error.",
                    Message = getWalletResult.Error.Message,
                };
                var instanceID = System.Guid.NewGuid().ToString();
                notification.InstanceID = instanceID;
                notification.CreatedDate = ServerTimeUTC;
                var sendResult = await TableNotificationAssistant.SendProfileNotificationAsync(profileId, notification,  60 * 60 * 24);
                await CurrencyModule.AddVirtualCurrencyToProfileAsync(profileId, "SC", amount);
                return ErrorHandler.ThrowError(getWalletResult.Error).AsFunctionResult();
            }

            var profileDictionary = getWalletResult.Result.Data;

            try {
                walletPrivateKey = profileDictionary[walletPrivateKeyKey].Value;
                walletAddress = profileDictionary[walletAddressKey].Value;
            } catch (Exception e) {
                var notification = new CBSNotification{
                    ID = 1.ToString(),
                    Read = false,
                    Title = "Transaction error.",
                    Message = "Wallet not found.",
                };
                var instanceID = System.Guid.NewGuid().ToString();
                notification.InstanceID = instanceID;
                notification.CreatedDate = ServerTimeUTC;
                var sendResult = await TableNotificationAssistant.SendProfileNotificationAsync(profileId, notification,  60 * 60 * 24);
                await CurrencyModule.AddVirtualCurrencyToProfileAsync(profileId, "SC", amount);
                return ErrorHandler.ThrowError(CBSError.FromMessage("Wallet not found.")).AsFunctionResult();
            }

            if (walletPrivateKey.Length == 0 || walletAddress.Length == 0) {
                var notification = new CBSNotification{
                    ID = 1.ToString(),
                    Read = false,
                    Title = "Transaction error.",
                    Message = "Wallet not found.",
                };
                var instanceID = System.Guid.NewGuid().ToString();
                notification.InstanceID = instanceID;
                notification.CreatedDate = ServerTimeUTC;
                var sendResult = await TableNotificationAssistant.SendProfileNotificationAsync(profileId, notification,  60 * 60 * 24);
                await CurrencyModule.AddVirtualCurrencyToProfileAsync(profileId, "SC", amount);
                return ErrorHandler.ThrowError(CBSError.FromMessage("Wallet not found.")).AsFunctionResult();
            }

            var signer = new Eip712TypedDataSigner();

            var typedData = EIP2612TypeFactory.GetTypedDefinition();

            typedData.Domain.Name = domainName;
            typedData.Domain.ChainId = chainID;
            typedData.Domain.VerifyingContract = contractAddress;

            

            var bankWalletKey = new EthECKey(bankPrivateKey);
            var playerWalletKey = new EthECKey(walletPrivateKey);


            var bankWeb3Account = new Nethereum.Web3.Accounts.Account(bankWalletKey.GetPrivateKey());
            var playerWeb3Account = new Nethereum.Web3.Accounts.Account(playerWalletKey.GetPrivateKey());
            

            var bankWeb3 = new Web3(bankWeb3Account, netUrl);
            var playerWeb3 = new Web3(playerWeb3Account, netUrl);

            var bankContract = bankWeb3.Eth.GetContract(contractABI, contractAddress);
            var playerContract = playerWeb3.Eth.GetContract(contractABI, contractAddress);

            var functionAllowance = bankContract.GetFunction("allowance");
            
            var functionAllowanceJson = @"{'owner': '" + bankAddress + "', 'spender': '" + walletAddress + "'}";
            var functionAllowanceValues = functionAllowance.ConvertJsonToObjectInputParameters(functionAllowanceJson);
            var allowanceAmount = await functionAllowance.CallAsync<BigInteger>(functionAllowanceValues.ToArray());
            

            var amountWei = Web3.Convert.ToWei(amount);
            var permitAmount = amountWei - allowanceAmount;

            if (permitAmount > 0) {
                try {
                    var functionNonce = bankContract.GetFunction("nonces");
                    var functionNonceJson = @"{'owner': '" + bankAddress + "'}";
                    var functionNonceValues = functionNonce.ConvertJsonToObjectInputParameters(functionNonceJson);
                    var nonce = await functionNonce.CallAsync<BigInteger>(functionNonceValues.ToArray());

                    var permit = new Permit
                    {
                        Owner = bankAddress,
                        Spender = walletAddress,
                        Value = permitAmount,
                        Nonce = nonce,
                        Deadline = new BigInteger(unchecked((uint)-1))
                    };


                    var signature = signer.SignTypedDataV4(permit, typedData, bankWalletKey);

                    var r = signature.Substring(0, 66);
                    var s = "0x" + signature.Substring(66, 130 - 66);
                    var v = Int32.Parse(signature.Substring(130, 132 - 130), System.Globalization.NumberStyles.HexNumber);

                    
                    

                    var functionPermit = playerContract.GetFunction("permit");
                   
                    var functionPermitJson = @"{'owner': '" + bankAddress + "', 'spender': '" + walletAddress + "', 'value': " + permitAmount.ToString() + ", 'deadline': " + new BigInteger(unchecked((uint)-1)) + ", 'v': " + v + ", 'r': '" + r + "', 's': '" + s + "'}";
                    var functionPermitValues = functionPermit.ConvertJsonToObjectInputParameters(functionPermitJson);
                    var estimatePermit = await functionPermit.EstimateGasAsync(playerWeb3Account.Address, null, null, functionPermitValues.ToArray());
                    
                    var receiptSending = await functionPermit.SendTransactionAndWaitForReceiptAsync(
                        playerWeb3Account.Address,
                        new HexBigInteger(100000), 
                        new HexBigInteger(Nethereum.Web3.Web3.Convert.ToWei(gasPrice, UnitConversion.EthUnit.Gwei)), 
                        null, 
                        System.Threading.CancellationToken.None,
                        functionPermitValues.ToArray()
                    ).ConfigureAwait(false);
                     
                } catch (Exception e) {
                    var notification = new CBSNotification{
                        ID = 1.ToString(),
                        Read = false,
                        Title = "Transaction error.",
                        Message = e.Message,
                    };
                    var instanceID = System.Guid.NewGuid().ToString();
                    notification.InstanceID = instanceID;
                    notification.CreatedDate = ServerTimeUTC;
                    var sendResult = await TableNotificationAssistant.SendProfileNotificationAsync(profileId, notification,  60 * 60 * 24);
                    await CurrencyModule.AddVirtualCurrencyToProfileAsync(profileId, "SC", amount);
                    return ErrorHandler.ThrowError(CBSError.FromMessage(e.Message)).AsFunctionResult();
                }
            }

            try {
                var functionTransferFrom = playerContract.GetFunction("transferFrom");
                var functionTransferFromJson = @"{'from': '" + bankAddress + "', 'to': '" + walletAddress + "', 'amount': " + amountWei.ToString() + "}";
                var functionTransferFromValues = functionTransferFrom.ConvertJsonToObjectInputParameters(functionTransferFromJson);
                var estimateTransferFrom = await functionTransferFrom.EstimateGasAsync(playerWeb3Account.Address, null, null, functionTransferFromValues.ToArray());
                log.LogInformation(estimateTransferFrom.ToString());
                var receiptSending = await functionTransferFrom.SendTransactionAndWaitForReceiptAsync(
                    playerWeb3Account.Address,
                    new HexBigInteger(100000), 
                    new HexBigInteger(Nethereum.Web3.Web3.Convert.ToWei(gasPrice, UnitConversion.EthUnit.Gwei)), 
                    null, 
                    System.Threading.CancellationToken.None,
                    functionTransferFromValues.ToArray()
                ).ConfigureAwait(false);

                var notification = new CBSNotification{
                    ID = 1.ToString(),
                    Read = false,
                    Title = "Transaction successful",
                    Message = "The transaction for the withdrawal of " + amount.ToString() + " W3BZ is successful. Transaction: " +  receiptSending.TransactionHash + ". Transaction status: " + receiptSending.Status.Value.ToString(),
                };

                var instanceID = System.Guid.NewGuid().ToString();
                notification.InstanceID = instanceID;
                notification.CreatedDate = ServerTimeUTC;
                var sendResult = await TableNotificationAssistant.SendProfileNotificationAsync(profileId, notification,  60 * 60 * 24);
            } catch (Exception e) {
                var notification = new CBSNotification{
                    ID = 1.ToString(),
                    Read = false,
                    Title = "Transaction error.",
                    Message = e.Message,
                };
                var instanceID = System.Guid.NewGuid().ToString();
                notification.InstanceID = instanceID;
                notification.CreatedDate = ServerTimeUTC;
                var sendResult = await TableNotificationAssistant.SendProfileNotificationAsync(profileId, notification,  60 * 60 * 24);
                await CurrencyModule.AddVirtualCurrencyToProfileAsync(profileId, "SC", amount);
                return ErrorHandler.ThrowError(CBSError.FromMessage(e.Message)).AsFunctionResult();
            }
            
            return string.Empty;
        }

        [FunctionName(AzureFunctions.PushToGameBalanceMethod)]
        public static async Task<dynamic> PushToGameBalanceMethodTrigger([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, [DurableClient] IDurableOrchestrationClient starter, ILogger log)
        {
            var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(await req.ReadAsStringAsync());
            var request = context.GetRequest<FunctionPushTokenRequest>();

            var profileID = request.ProfileID;
            var code = "SC";
            var amount = request.amount;
            string walletAddress, walletPrivateKey;

            var profileDataMap = new string[] 
            {
                walletAddressKey,
                walletPrivateKeyKey,
                incomeTransactionHashKey
            };

            var getWalletResult = await ProfileModule.GetProfileDataAsync(profileID, profileDataMap);

            if (getWalletResult.Error != null) {
                return ErrorHandler.ThrowError(getWalletResult.Error).AsFunctionResult();
            }

            var profileDictionary = getWalletResult.Result.Data;

            try {
                walletAddress = profileDictionary[walletAddressKey].Value;
                walletPrivateKey = profileDictionary[walletPrivateKeyKey].Value;
            } catch (Exception e) {
                return ErrorHandler.ThrowError(CBSError.FromMessage("No withdrawal wallet found.")).AsFunctionResult();
            }

            if (walletAddress.Length == 0 || walletPrivateKey.Length == 0) {
                return ErrorHandler.ThrowError(CBSError.FromMessage("No withdrawal wallet found.")).AsFunctionResult();
            }

            try {
                var transactionHash = profileDictionary[incomeTransactionHashKey].Value;
                if (transactionHash.Length != 0) {
                    return ErrorHandler.ThrowError(CBSError.FromMessage("Wait for the completion of the previous application for replenishment.")).AsFunctionResult();
                }
            } catch (Exception e) {}

            try {
                var account = new Nethereum.Web3.Accounts.Account(walletPrivateKey);
                var web3 = new Web3(account, netUrl);
                var transferHandler = web3.Eth.GetContractTransactionHandler<TransferFunction>();
                var transfer = new TransferFunction()
                {
                    To = ownerWalletAddress,
                    Value = Nethereum.Web3.Web3.Convert.ToWei(amount)
                };
                transfer.GasPrice =  Nethereum.Web3.Web3.Convert.ToWei(gasPrice, UnitConversion.EthUnit.Gwei);
                var estimate = await transferHandler.EstimateGasAsync(contractAddress, transfer);
                transfer.Gas = estimate.Value;
                var transactionReceipt = await transferHandler.SendRequestAndWaitForReceiptAsync(contractAddress, transfer);
                await ProfileModule.SetProfileDataAsync(profileID, incomeTransactionHashKey, transactionReceipt.TransactionHash);
                await ProfileModule.SetProfileDataAsync(profileID, incomeTransactionAmountKey, amount.ToString());
                await ProfileModule.SetProfileDataAsync(profileID, incomeTransactionCurrencyKey, code);
                await ProfileModule.SetProfileDataAsync(profileID, incomeTransactionStatusKey, transactionReceipt.Status.Value.ToString());
                var notification = new CBSNotification{
                    ID = 1.ToString(),
                    Read = false, 
                    Title = "Transaction created",
                    Message = "A transaction for replenishment of " + amount.ToString() + " W3BZ has been created. Transaction: " +  transactionReceipt.TransactionHash + ". Transaction status: " + transactionReceipt.Status.Value.ToString(),
                };
                var instanceID = System.Guid.NewGuid().ToString();
                notification.InstanceID = instanceID;
                notification.CreatedDate = ServerTimeUTC;
                var sendResult = await TableNotificationAssistant.SendProfileNotificationAsync(profileID, notification,  60 * 60 * 24);
                var requestCheck = new FunctionCheckTransactionRequest{
                    ProfileID = profileID,
                    transactionHash = transactionReceipt.TransactionHash
                };
                string requestRaw = JsonPlugin.ToJsonCompress(requestCheck);
                var taskID = await starter.StartNewAsync(AzureFunctions.CheckPushToGameBalanceTransactionProcessMethod, null, requestRaw);
            } catch (Exception e) {
                return ErrorHandler.ThrowError(CBSError.FromMessage("Unexpected error. Check your BNB and W3BZ wallet balance.")).AsFunctionResult();
            }

            var Result = new FunctionPushTokenResult
                {
                    Message = "The replenishment transaction has been successfully created."
                };

            return Result.AsFunctionResult();
        }

        [FunctionName(AzureFunctions.CheckPushToGameBalanceTransactionMethod)]
        public static async Task<string> CheckPushToGameBalanceTransaction([ActivityTrigger] string requestContent, ILogger log)
        {
            var request = JsonPlugin.FromJsonDecompress<FunctionCheckTransactionRequest>(requestContent);
            var profileId = request.ProfileID;
            var transactionHash = request.transactionHash;
            var amount = 0;

log.LogDebug(requestContent);
            log.LogDebug(profileId);
            log.LogDebug(transactionHash);
            log.LogInformation(profileId);
            log.LogInformation(transactionHash);
            log.LogInformation(requestContent);

            await ProfileModule.SetProfileDataAsync(request.ProfileID, "func", "CheckPushToGameBalanceTransaction");

            var profileDataMap = new string[] 
            {
                incomeTransactionAmountKey
            };

            var getWalletResult = await ProfileModule.GetProfileDataAsync(profileId, profileDataMap);

            if (getWalletResult.Error != null) {
                return ErrorHandler.ThrowError(getWalletResult.Error).AsFunctionResult();
            }

            var profileDictionary = getWalletResult.Result.Data;

            try {
                amount = Int32.Parse(profileDictionary[incomeTransactionAmountKey].Value);
            } catch (Exception e) {
                return ErrorHandler.ThrowError(CBSError.FromMessage("No withdrawal wallet found.")).AsFunctionResult();
            }

            var account = new Nethereum.Web3.Accounts.Account(ownerWalletPrivateKey);
            var web3 = new Web3(account, netUrl);
            var receipt = await web3.Eth.Transactions.GetTransactionReceipt.SendRequestAsync(transactionHash);

            if (receipt.Status.Value == 1) {
                    await ProfileModule.SetProfileDataAsync(profileId, incomeTransactionHashKey, "");
                    await ProfileModule.SetProfileDataAsync(profileId, incomeTransactionAmountKey, "");
                    await ProfileModule.SetProfileDataAsync(profileId, incomeTransactionCurrencyKey, "");
                    await ProfileModule.SetProfileDataAsync(profileId, incomeTransactionStatusKey, "");

                    await CurrencyModule.AddVirtualCurrencyToProfileAsync(profileId, "SC", amount);
                    
                    var notification = new CBSNotification{
                        ID = 1.ToString(),
                        Read = false,
                        Title = "Transaction successful",
                        Message = "The transaction for replenishment of " + amount.ToString() + " W3BZ is successful. Funds credited to the wallet. Transaction: " +  receipt.TransactionHash + ". Transaction status: " + receipt.Status.Value.ToString(),
                    };

                    var instanceID = System.Guid.NewGuid().ToString();
                    notification.InstanceID = instanceID;
                    notification.CreatedDate = ServerTimeUTC;
                    var sendResult = await TableNotificationAssistant.SendProfileNotificationAsync(profileId, notification, 60 * 60 * 24);
                }
            
            return string.Empty;
        }

        [FunctionName(AzureFunctions.CheckPushToGameBalanceTransactionProcessMethod)]
        public static async Task CheckPushToGameBalanceTransactionMethodProcess([OrchestrationTrigger] IDurableOrchestrationContext context)
        {
            var requestContent = context.GetInput<string>();
            DateTime waitAWhile = context.CurrentUtcDateTime.Add(TimeSpan.FromSeconds(5));
            await context.CreateTimer(waitAWhile, CancellationToken.None);
            await context.CallActivityAsync(AzureFunctions.CheckPushToGameBalanceTransactionMethod, requestContent);
        }




        
    }
}
