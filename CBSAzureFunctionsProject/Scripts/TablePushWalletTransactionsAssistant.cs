using Azure.Data.Tables;
using CBS.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace CBS
{
    public class TablePushWalletTransactionsAssistant
    {
        private static readonly string TableID = "CBSPushToWalletTransactions";
        private static readonly string ProfileIdKey = "ProfileID";
        private static readonly string TransationHashKey = "TransationHash";
        private static readonly string AmountKey = "Amount";
        private static readonly int MaxLogsHistory = 100;

        public static async Task<ExecuteResult<FunctionEmptyResult>> AddTransactionAsync(string ProfileId, string TransactionHash, int amount)
        {
            var partitionKey = TicksKey();
            var entity = new TableEntity();
            entity.PartitionKey = partitionKey;
            entity.RowKey = ProfileId;
            entity[ProfileIdKey] = ProfileId;
            entity[TransationHashKey] = TransactionHash;
            entity[AmountKey] = amount;
            var addResult = await StorageTable.AddEntityAsync(TableID, entity);
            if (addResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionEmptyResult>(addResult.Error);
            }
            return new ExecuteResult<FunctionEmptyResult>
            {
                Result = new FunctionEmptyResult()
            };
        }

        public static async Task<ExecuteResult<FunctionGetTransactionsResult>> GetTransactionsAsync()
        {
            var getEntityResult = await StorageTable.GetTopFromTableAndSaveLastNAsync(TableID, MaxLogsHistory);
            if (getEntityResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionGetEventLogResult>(getEntityResult.Error);
            }
            var entities = getEntityResult.Result;
            var transactionList = new List<Transaction>();
            foreach (var entity in entities)
            {
                transactionList.Add(new Transaction{
                    ProfileID = entity.GetString(ProfileIdKey),
                    TransactionHash = entity.GetString(TransationHashKey),
                    amount = entity.GetInt32(AmountKey) ?? 0,
                    entity = entity
                });
            }
            return new ExecuteResult<FunctionGetTransactionsResult>
            {
                Result = new FunctionGetTransactionsResult
                {
                    transactions = transactionList
                }
            };
        }

        public static async Task<ExecuteResult<FunctionEmptyResult>> DeleteTransactionAsync(Transaction transaction)
        {
            await StorageTable.DeleteEntityAsync(TableID, transaction.entity);
            return new ExecuteResult<FunctionEmptyResult>
            {
                Result = new FunctionEmptyResult()
            };
        }

        public static string TicksKey()
        {
            return (DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks).ToString("d19");
        }

        
    }
}