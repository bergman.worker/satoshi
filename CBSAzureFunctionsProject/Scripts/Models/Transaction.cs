using System;
using Azure.Data.Tables;

namespace CBS.Models
{
    public class Transaction
    {
        public string ProfileID;
        public string TransactionHash;
        public int amount;

        public TableEntity entity;
    }
}