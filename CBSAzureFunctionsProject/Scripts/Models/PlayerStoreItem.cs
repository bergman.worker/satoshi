using System;
using Azure.Data.Tables;

namespace CBS.Models
{
    public class PlayerStoreItem
    {
        public string ProfileID;
        public string ItemInstanceId;
        public string ItemId;
        public string TradeId;
        public int price;
        public TableEntity entity;
    }
}