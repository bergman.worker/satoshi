using System;
using Azure.Data.Tables;

namespace CBS.Models
{
    public class PVP
    {
        public string Id;
        public string CreatorId;
        public string OpponentId;
        public int bet;
        public string Status;

        public TableEntity entity;
    }
}