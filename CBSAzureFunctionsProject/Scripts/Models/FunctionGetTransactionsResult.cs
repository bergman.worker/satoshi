
using System.Collections.Generic;

namespace CBS.Models
{
    public class FunctionGetTransactionsResult
    {
        public List<Transaction> transactions;
    }
}
