
using System.Collections.Generic;

namespace CBS.Models
{
    public class FunctionGetStoreResult
    {
        public List<PlayerStoreItem> storeItems;
    }
}
