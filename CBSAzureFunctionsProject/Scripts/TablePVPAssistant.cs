using Azure.Data.Tables;
using CBS.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using DurableTask.Core.Common;


namespace CBS
{
    public class TablePVPAssistant
    {
        private static readonly string TableID = "CBSPVP";
        private static readonly string CreatorIdKey = "CreatorId";
        private static readonly string OpponentIdKey = "OpponentId";
        private static readonly string BetKey = "Bet";
        private static readonly string StatusKey = "Status";

        private static Random random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static async Task<ExecuteResult<FunctionGetPVPResult>> CreatePVP(string CreatorId, string OpponentId, int bet)
        {
            var queryResult = await StorageTable.QueryEntitiesAsync(TableID, new string[] {"PartitionKey", 
                "RowKey", CreatorIdKey, OpponentIdKey, BetKey, StatusKey}, CreatorIdKey + " eq '" + CreatorId + "' and " + StatusKey + " eq 'NEW'");
            if (queryResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionEmptyResult>(queryResult.Error);
            }   
            var entities = queryResult.Result; 
            foreach (var entity in entities) {
                await StorageTable.DeleteEntityAsync(TableID, entity);
            }
            var partitionKey = TicksKey();
            var newEntity = new TableEntity();
            newEntity.PartitionKey = partitionKey;
            newEntity.RowKey = RandomString(10);
            newEntity[CreatorIdKey] = CreatorId;
            newEntity[OpponentIdKey] = OpponentId;
            newEntity[BetKey] = bet;
            newEntity[StatusKey] = "NEW";
            var addResult = await StorageTable.AddEntityAsync(TableID, newEntity);
            if (addResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionEmptyResult>(addResult.Error);
            }
            var pvp = new PVP()
            {
                Id = newEntity.RowKey,
                CreatorId = CreatorId,
                OpponentId = OpponentId,
                bet = bet,
                Status = "NEW"
            };
            return new ExecuteResult<FunctionGetPVPResult>
            {
                Result = new FunctionGetPVPResult()
                {
                    pvp = pvp
                }
            };
        }

        public static async Task<ExecuteResult<FunctionGetPVPResult>> GetPVPList(string ProfileId)
        {
            var queryCanceledResult = await StorageTable.QueryEntitiesAsync(TableID, new string[] {"PartitionKey", 
                "RowKey", CreatorIdKey, OpponentIdKey, BetKey, StatusKey}, CreatorIdKey + " eq '" + ProfileId + "' and " + StatusKey + " eq 'CANCELED'");

            if (queryCanceledResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionGetPVPResult>(queryCanceledResult.Error);
            } 

            var canceledEntities = queryCanceledResult.Result; 

            foreach (var canceledEntity in canceledEntities) {
                var pvp = new PVP()
                {
                    Id = canceledEntity.RowKey,
                    CreatorId = canceledEntity.GetString(CreatorIdKey),
                    OpponentId = canceledEntity.GetString(OpponentIdKey),
                    bet = canceledEntity.GetInt32(BetKey) ?? 0,
                    Status = canceledEntity.GetString(StatusKey)
                };
                await StorageTable.DeleteEntityAsync(TableID, canceledEntity);
                return new ExecuteResult<FunctionGetPVPResult>
                {
                    Result = new FunctionGetPVPResult()
                    {
                        pvp = pvp
                    }
                };
            }

            var queryAcceptedResult = await StorageTable.QueryEntitiesAsync(TableID, new string[] {"PartitionKey", 
                "RowKey", CreatorIdKey, OpponentIdKey, BetKey, StatusKey}, CreatorIdKey + " eq '" + ProfileId + "' and " + StatusKey + " eq 'ACCEPTED'");
            if (queryAcceptedResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionGetPVPResult>(queryAcceptedResult.Error);
            }   
            var acceptedEntities = queryAcceptedResult.Result; 
            foreach (var acceptedEntity in acceptedEntities) {
                var pvp = new PVP()
                {
                    Id = acceptedEntity.RowKey,
                    CreatorId = acceptedEntity.GetString(CreatorIdKey),
                    OpponentId = acceptedEntity.GetString(OpponentIdKey),
                    bet = acceptedEntity.GetInt32(BetKey) ?? 0,
                    Status = acceptedEntity.GetString(StatusKey)
                };
                //await StorageTable.DeleteEntityAsync(TableID, acceptedEntity);
                return new ExecuteResult<FunctionGetPVPResult>
                {
                    Result = new FunctionGetPVPResult()
                    {
                        pvp = pvp
                    }
                };
            }

            var queryNewResult = await StorageTable.QueryEntitiesAsync(TableID, new string[] {"PartitionKey", 
                "RowKey", CreatorIdKey, OpponentIdKey, BetKey, StatusKey}, OpponentIdKey + " eq '" + ProfileId + "' and " + StatusKey + " eq 'NEW'");
            if (queryNewResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionGetPVPResult>(queryNewResult.Error);
            }   
            var newEntities = queryNewResult.Result; 
            foreach (var newEntity in newEntities) {
                var pvp = new PVP()
                {
                    Id = newEntity.RowKey,
                    CreatorId = newEntity.GetString(CreatorIdKey),
                    OpponentId = newEntity.GetString(OpponentIdKey),
                    bet = newEntity.GetInt32(BetKey) ?? 0,
                    Status = newEntity.GetString(StatusKey)
                };
                return new ExecuteResult<FunctionGetPVPResult>
                {
                    Result = new FunctionGetPVPResult()
                    {
                        pvp = pvp
                    }
                };
            }
          
            return ErrorHandler.ThrowError<FunctionGetPVPResult>(CBSError.FromMessage("No pvp"));
        }

        public static async Task<ExecuteResult<FunctionGetPVPResult>> AcceptPVP(string pvpId)
        {
            var queryResult = await StorageTable.QueryEntitiesAsync(TableID, new string[] {"PartitionKey", 
                "RowKey", CreatorIdKey, OpponentIdKey, BetKey, StatusKey}, "RowKey eq '" + pvpId + "'");
            if (queryResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionGetPVPResult>(queryResult.Error);
            }   
            var entities = queryResult.Result; 
            if (entities.Count == 0) {
                return ErrorHandler.ThrowError<FunctionGetPVPResult>(CBSError.FromMessage("Game not found."));
            }
            PVP pvp = null;
            foreach (var entity in entities) {
                entity[StatusKey] = "ACCEPTED";
                await StorageTable.UpdateEntityAsync(TableID, entity);
                pvp = new PVP()
                {
                    Id = entity.RowKey,
                    CreatorId = entity.GetString(CreatorIdKey),
                    OpponentId = entity.GetString(OpponentIdKey),
                    bet = entity.GetInt32(BetKey) ?? 0,
                    Status = entity.GetString(StatusKey)
                };
            }
            return new ExecuteResult<FunctionGetPVPResult>
            {
                Result = new FunctionGetPVPResult()
                {
                    pvp = pvp
                }
            };
        }

        public static async Task<ExecuteResult<FunctionGetPVPResult>> DeclinePVP(string pvpId)
        {
            var queryResult = await StorageTable.QueryEntitiesAsync(TableID, new string[] {"PartitionKey", 
                "RowKey", CreatorIdKey, OpponentIdKey, BetKey, StatusKey}, "RowKey eq '" + pvpId + "'");
            if (queryResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionGetPVPResult>(queryResult.Error);
            }   
            var entities = queryResult.Result; 
            if (entities.Count == 0) {
                return ErrorHandler.ThrowError<FunctionGetPVPResult>(CBSError.FromMessage("Game not found."));
            }
            PVP pvp = null;
            foreach (var entity in entities) {
                entity[StatusKey] = "CANCELED";
                await StorageTable.UpdateEntityAsync(TableID, entity);
                pvp = new PVP()
                {
                    Id = entity.RowKey,
                    CreatorId = entity.GetString(CreatorIdKey),
                    OpponentId = entity.GetString(OpponentIdKey),
                    bet = entity.GetInt32(BetKey) ?? 0,
                    Status = entity.GetString(StatusKey)
                };
            }
            
            return new ExecuteResult<FunctionGetPVPResult>
            {
                Result = new FunctionGetPVPResult()
                {
                    pvp = pvp
                }
            };
        }

        public static async Task<ExecuteResult<FunctionGetPVPResult>> CancelPVP(string pvpId)
        {
            var queryResult = await StorageTable.QueryEntitiesAsync(TableID, new string[] {"PartitionKey", 
                "RowKey", CreatorIdKey, OpponentIdKey, BetKey, StatusKey}, "RowKey eq '" + pvpId + "'");
            if (queryResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionGetPVPResult>(queryResult.Error);
            }   
            var entities = queryResult.Result; 
            if (entities.Count == 0) {
                return ErrorHandler.ThrowError<FunctionGetPVPResult>(CBSError.FromMessage("Game not found."));
            }
            foreach (var entity in entities) {
                await StorageTable.DeleteEntityAsync(TableID, entity);
            }
            
            return new ExecuteResult<FunctionGetPVPResult>
            {
                Result = new FunctionGetPVPResult()
                {
                    pvp = new PVP()
                }
            };
        }

        public static async Task<ExecuteResult<FunctionGetPVPResult>> StartPVP(string pvpId)
        {
            var queryResult = await StorageTable.QueryEntitiesAsync(TableID, new string[] {"PartitionKey", 
                "RowKey", CreatorIdKey, OpponentIdKey, BetKey, StatusKey}, "RowKey eq '" + pvpId + "'");
            if (queryResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionEmptyResult>(queryResult.Error);
            }   
            var entities = queryResult.Result; 
            PVP pvp = null;
            foreach (var entity in entities) {
                entity[StatusKey] = "FIGHTING";
                await StorageTable.UpdateEntityAsync(TableID, entity);
                pvp = new PVP()
                {
                    Id = entity.RowKey,
                    CreatorId = entity.GetString(CreatorIdKey),
                    OpponentId = entity.GetString(OpponentIdKey),
                    bet = entity.GetInt32(BetKey) ?? 0,
                    Status = entity.GetString(StatusKey)
                };
            }
            return new ExecuteResult<FunctionGetPVPResult>
            {
                Result = new FunctionGetPVPResult()
                {
                    pvp = pvp
                }
            };
        }

        public static async Task<ExecuteResult<FunctionGetPVPResult>> GetPVP(string pvpId)
        {
            var queryResult = await StorageTable.QueryEntitiesAsync(TableID, new string[] {"PartitionKey", 
                "RowKey", CreatorIdKey, OpponentIdKey, BetKey, StatusKey}, "RowKey eq '" + pvpId + "'");
            if (queryResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionEmptyResult>(queryResult.Error);
            }   
            var entities = queryResult.Result; 
            PVP pvp = null;
            foreach (var entity in entities) {
                pvp = new PVP()
                {
                    Id = entity.RowKey,
                    CreatorId = entity.GetString(CreatorIdKey),
                    OpponentId = entity.GetString(OpponentIdKey),
                    bet = entity.GetInt32(BetKey) ?? 0,
                    Status = entity.GetString(StatusKey),
                    entity = entity
                };
            }
            return new ExecuteResult<FunctionGetPVPResult>
            {
                Result = new FunctionGetPVPResult()
                {
                    pvp = pvp
                }
            };
        }

        public static async Task<ExecuteResult<FunctionEmptyResult>> FinishPVP(TableEntity entity) {
            entity[StatusKey] = "FINISHED";
            await StorageTable.UpdateEntityAsync(TableID, entity);

            return new ExecuteResult<FunctionEmptyResult>
            {
                Result = new FunctionEmptyResult()
                {
                }
            };
        }

        /*public static async Task<ExecuteResult<FunctionGetTransactionsResult>> GetTransactionsAsync()
        {
            var getEntityResult = await StorageTable.GetTopFromTableAndSaveLastNAsync(TableID, MaxLogsHistory);
            if (getEntityResult.Error != null)
            {
                return ErrorHandler.ThrowError<FunctionGetEventLogResult>(getEntityResult.Error);
            }
            var entities = getEntityResult.Result;
            var transactionList = new List<Transaction>();
            foreach (var entity in entities)
            {
                transactionList.Add(new Transaction{
                    ProfileID = entity.GetString(ProfileIdKey),
                    TransactionHash = entity.GetString(TransationHashKey),
                    amount = entity.GetInt32(AmountKey) ?? 0,
                    entity = entity
                });
            }
            return new ExecuteResult<FunctionGetTransactionsResult>
            {
                Result = new FunctionGetTransactionsResult
                {
                    transactions = transactionList
                }
            };
        }

        public static async Task<ExecuteResult<FunctionEmptyResult>> DeleteTransactionAsync(Transaction transaction)
        {
            await StorageTable.DeleteEntityAsync(TableID, transaction.entity);
            return new ExecuteResult<FunctionEmptyResult>
            {
                Result = new FunctionEmptyResult()
            };
        }*/

        public static string TicksKey()
        {
            return (DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks).ToString("d19");
        }

        
    }
}