﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UFE3D;
using CBS;
using CBS.UI;
using CBS.Models;
using System.Linq;
using Satoshi.Animation;

public class DefaultMainMenuScreen : MainMenuScreen{
	private ICBSInventory Inventory { get; set; }
	private IProfile Profile { get; set; }
	private ICurrency CurrencyModule { get; set; }

	private IMatchmaking Matchmaking { get; set; }
	private string CharacterName = "Boxer";
	private PopupViewer popupViewer;
	#region public instance fields
	public AudioClip onLoadSound;
	public AudioClip music;
	public AudioClip selectSound;
	public AudioClip cancelSound;
	public AudioClip moveCursorSound;
	public bool stopPreviousSoundEffectsOnLoad = false;
	public float delayBeforePlayingMusic = 0.1f;

	public Button buttonNetwork;
	public Button buttonBluetooth;
	public Toggle toggle100;
	public Toggle toggle500;
	public Toggle toggle1000;
	public Toggle casinoToggle100;
	public Toggle casinoToggle500;
	public Toggle casinoToggle1000;
	public Toggle casinoToggleLeftAI;
	public Toggle casinoToggleRightAI;
	#endregion

	private GameObject optionScreen;

	#region public override methods
	public override void DoFixedUpdate(
		IDictionary<InputReferences, InputEvents> player1PreviousInputs,
		IDictionary<InputReferences, InputEvents> player1CurrentInputs,
		IDictionary<InputReferences, InputEvents> player2PreviousInputs,
		IDictionary<InputReferences, InputEvents> player2CurrentInputs
	){
		base.DoFixedUpdate(player1PreviousInputs, player1CurrentInputs, player2PreviousInputs, player2CurrentInputs);

		this.DefaultNavigationSystem(
			player1PreviousInputs,
			player1CurrentInputs,
			player2PreviousInputs,
			player2CurrentInputs,
			this.moveCursorSound,
			this.selectSound,
			this.cancelSound
		);
	}

    public void Start()
    {
		Matchmaking = CBSModule.Get<CBSMatchmakingModule>();
		Matchmaking.OnStatusChanged += OnStatusChanged;

		optionScreen = Instantiate(UFE.config.gameGUI.optionsScreen.gameObject, transform);
		optionScreen.SetActive(false);
	}

	public void ShwOptionsScreen()
	{
		AnimationSettingsConfigurator.instance.InAnimation(optionScreen.transform);
	}

    public void OnDestroy()
    {
		Matchmaking.OnStatusChanged -= OnStatusChanged;
	}

    private void OnStatusChanged(CBSCheckPVPResult matchInfo)
	{
		if (matchInfo == null) return;
		if (matchInfo.Status == "NEW")
        {
			var yesOrNoPopupRequest = new YesOrNoPopupRequest()
			{
				Body = "Player " + matchInfo.CreatorName + " invites you to fight for " + matchInfo.bet + " tokens. Press \"Yes\" to accept or \"No\" to decline.",
				Title = "PVP Request",
				OnNoAction = OnDecline,
				OnYesAction = OnAccept
			};
			popupViewer = new PopupViewer();
			popupViewer.ShowYesNoPopup(yesOrNoPopupRequest);
		}
		if (matchInfo.Status == "CANCELED")
        {
			new PopupViewer().ShowSimplePopup(new PopupRequest
			{
				Title = "PVP Request",
				Body = "Player " + matchInfo.OpponentName + " declined your request to fight.",
			});
		}
		if (matchInfo.Status == "ACCEPTED")
        {
			OnOpponentAccept();
        }
	}

	public void OnOpponentAccept()
	{
		popupViewer = new PopupViewer();
		popupViewer.ShowLoadingPopup();
		Matchmaking = CBSModule.Get<CBSMatchmakingModule>();
		Matchmaking.StartPVP(Matchmaking.MatchInfo.pvpId, OnStarted);
	}

	public void OnStarted(CBSBaseResult result)
	{
		popupViewer.HideLoadingPopup();
		if (!result.IsSuccess)
		{
			new PopupViewer().ShowSimplePopup(new PopupRequest
			{
				Title = "Error",
				Body = result.Error.Message
			});
		}
		else
		{
			popupViewer = new PopupViewer();
			popupViewer.ShowLoadingPopup();
			Inventory = CBSModule.Get<CBSInventoryModule>();
			Inventory.GetInventory(OnGetInventoryAndJoinToPVP);
		}
	}

	private void OnGetInventoryAndJoinToPVP(CBSGetInventoryResult result)
	{
		popupViewer.HideLoadingPopup();
		if (result.IsSuccess)
		{
			var items = result.AllItems;
			var equippedItems = result.EquippedItems;
			List<UFE3D.CharacterInfo> characters = new List<UFE3D.CharacterInfo>();
			for (int i = 0; i < UFE.config.characters.Length; ++i)
			{
				characters.Add(UFE.config.characters[i]);
			}

			setupRig(equippedItems);
			
			foreach (var item in equippedItems)
			{
				if (item.Category == "Hero")
				{
					CharacterName = item.DisplayName;
				}
			}
			for (int i = 0; i < characters.Count; ++i)
			{
				if (characters[i].characterName == CharacterName)
				{
					UFE.networkCharacterIndex = i;
				}
			}
			var popup = GameObject.Find("SimplePopup(Clone)");
     		 var profile = GameObject.Find("ProfileInfo(Clone)");
     		 var friends = GameObject.Find("FriendsWindow(Clone)");
			 var leaderboards = GameObject.Find("LeaderboardsWindow(Clone)");
     		 if (popup) popup.SetActive(false);
     		 if (profile) profile.SetActive(false);
    		 if (friends) friends.SetActive(false);
			 if (leaderboards) leaderboards.SetActive(false);
			System.Random randomStage = new System.Random();
			int stageId = randomStage.Next(1, UFE.config.stages.Length - 1);
			UFE.networkStageIndex = stageId;
			UFE.StartJoinGameScreen();
		}
	}

	public void OnAccept()
    {
		popupViewer = new PopupViewer();
		popupViewer.ShowLoadingPopup();
		Inventory = CBSModule.Get<CBSInventoryModule>();
		Inventory.GetInventory(OnGetInventoryAndGoToPVP);
	}

	private void OnGetInventoryAndGoToPVP(CBSGetInventoryResult result)
	{
		popupViewer.HideLoadingPopup();
		if (result.IsSuccess)
		{
			var items = result.AllItems;
			var equippedItems = result.EquippedItems;
			List<UFE3D.CharacterInfo> characters = new List<UFE3D.CharacterInfo>();
			for (int i = 0; i < UFE.config.characters.Length; ++i)
			{
				characters.Add(UFE.config.characters[i]);
			}

			setupRig(equippedItems);
			
			foreach (var item in equippedItems)
			{
				if (item.Category == "Hero")
				{
					CharacterName = item.DisplayName;
				}
			}
			for (int i = 0; i < characters.Count; ++i)
			{
				if (characters[i].characterName == CharacterName)
				{
					UFE.networkCharacterIndex = i;
				}
			}
			System.Random randomStage = new System.Random();
			int stageId = randomStage.Next(1, UFE.config.stages.Length - 1);
			UFE.networkStageIndex = stageId;
			UFE.StartHostGameScreen();
		}
	}

	public void OnDecline()
	{
		popupViewer = new PopupViewer();
		popupViewer.ShowLoadingPopup();
		Matchmaking.DeclinePVP(Matchmaking.MatchInfo.pvpId, OnDeclined);
	}

	public void OnDeclined(CBSBaseResult result)
	{
		popupViewer.HideLoadingPopup();
		if (!result.IsSuccess)
		{
			new PopupViewer().ShowSimplePopup(new PopupRequest
			{
				Title = "Error",
				Body = result.Error.Message
			});
		}
	}

	public override void GoToTrainingModeScreen()
	{
		popupViewer = new PopupViewer();
		popupViewer.ShowLoadingPopup();
		Inventory = CBSModule.Get<CBSInventoryModule>();
		Inventory.GetInventory(OnGetInventoryAndGoToTraining);
	}

	public override void GoToStoryModeScreen()
	{
		popupViewer = new PopupViewer();
		popupViewer.ShowLoadingPopup();
		Inventory = CBSModule.Get<CBSInventoryModule>();
		Inventory.GetInventory(OnGetInventoryAndGoToStoryModeScreen);
	}

	public virtual void GoToVersusModeScreen()
	{
		popupViewer = new PopupViewer();
		popupViewer.ShowLoadingPopup();
		Matchmaking = CBSModule.Get<CBSMatchmakingModule>();
		Profile = CBSModule.Get<CBSProfileModule>();
		Matchmaking.StartVersusBattle(Profile.ProfileID, UFE.gameBet, OnStartedVersusBattle);
	}

	public virtual void GoToCasinoModeScreen()
	{
		popupViewer = new PopupViewer();
		popupViewer.ShowLoadingPopup();
		Matchmaking = CBSModule.Get<CBSMatchmakingModule>();
		Profile = CBSModule.Get<CBSProfileModule>();
		Matchmaking.StartCasinoBattle(Profile.ProfileID, UFE.gameBet, OnStartedCasinoBattle);
	}

	private void OnStartedCasinoBattle(CBSBaseResult result)
    {
		popupViewer.HideLoadingPopup();
		if (result.IsSuccess)
		{
			float fadeTime = (float)UFE.config.gameGUI.screenFadeDuration;
			UFE.gameMode = GameMode.VersusMode;
			UFE.SetCPU(1, true);
			UFE.SetCPU(2, true);
			List<UFE3D.CharacterInfo> characters = new List<UFE3D.CharacterInfo>();
			for (int i = 0; i < UFE.config.characters.Length; ++i)
			{
				characters.Add(UFE.config.characters[i]);
			}
			System.Random randomCharacter = new System.Random();
			int characterId1 = randomCharacter.Next(0, characters.Count - 1);
			int characterId2 = randomCharacter.Next(0, characters.Count - 1);
			UFE.SetPlayer(1, characters[characterId1]);
			UFE.SetPlayer(2, characters[characterId2]);
			System.Random randomStage = new System.Random();
			int stageId = randomStage.Next(1, UFE.config.stages.Length - 1);
			UFE.SetStage(UFE.config.stages[stageId]);
			UFE.StartLoadingBattleScreen();
		}
		else
		{
			new PopupViewer().ShowSimplePopup(new PopupRequest
			{
				Title = "Error",
				Body = result.Error.Message
			});
		}
		
	}

	public void GoToCasino()
    {
		UFE.gameBet = 0;
		var bet = 100;
		if (casinoToggle100.isOn)
		{
			bet = 100;
		}
		if (casinoToggle500.isOn)
		{
			bet = 500;
		}
		if (casinoToggle1000.isOn)
		{
			bet = 1000;
		}
		if (casinoToggleLeftAI.isOn)
        {
			UFE.casinoBetPlayer = 1;
        }
		if (casinoToggleRightAI.isOn)
		{
			UFE.casinoBetPlayer = 2;
		}
		UFE.gameBet = bet;
		GoToCasinoModeScreen();
	}

	public void GoToPVP()
    {
		UFE.gameBet = 0;
		var bet = 100;
		if (toggle100.isOn)
        {
			bet = 100;
        }
		if (toggle500.isOn)
        {
			bet = 250;
        }
		if (toggle1000.isOn)
        {
			bet = 500;
        }
		UFE.gameBet = bet;
		GoToVersusModeScreen();
	}

	public void GoToP2E()
    {
		UFE.gameBet = 0;
		GoToVersusModeScreen();
	}

	private void OnStartedVersusBattle(CBSBaseResult result)
	{
		popupViewer.HideLoadingPopup();
		if (result.IsSuccess)
		{
			popupViewer = new PopupViewer();
			popupViewer.ShowLoadingPopup();
			Inventory = CBSModule.Get<CBSInventoryModule>();
			Inventory.GetInventory(OnGetInventoryAndGoToPVE);
			
		}
		else
		{
			new PopupViewer().ShowSimplePopup(new PopupRequest
			{
				Title = "Error",
				Body = result.Error.Message
			});
		}
	}

	private void OnGetInventoryAndGoToTraining(CBSGetInventoryResult result)
	{
		popupViewer.HideLoadingPopup();
		if (result.IsSuccess)
		{
			var items = result.AllItems;
			var equippedItems = result.EquippedItems;
			float fadeTime = (float)UFE.config.gameGUI.screenFadeDuration;
			UFE.gameMode = GameMode.TrainingRoom;
			UFE.SetCPU(1, false);
			UFE.SetCPU(2, false);
			List<UFE3D.CharacterInfo> characters = new List<UFE3D.CharacterInfo>();
			for (int i = 0; i < UFE.config.characters.Length; ++i)
			{
				characters.Add(UFE.config.characters[i]);
			}

			setupRig(equippedItems);
			
			foreach (var item in equippedItems)
			{
				if (item.Category == "Hero")
				{
					CharacterName = item.DisplayName;
				}
			}
			for (int i = 0; i < characters.Count; ++i)
			{
				if (characters[i].characterName == CharacterName)
				{
					UFE.SetPlayer(1, characters[i]);
				}
			}
			System.Random random = new System.Random();
			int characterId = random.Next(0, characters.Count - 1);
			UFE.SetPlayer(2, characters[characterId]);
			UFE.SetStage(UFE.config.stages[0]);
			UFE.StartLoadingBattleScreen();
		}
	}

	private void OnGetInventoryAndGoToPVE(CBSGetInventoryResult result)
	{
		popupViewer.HideLoadingPopup();
		if (result.IsSuccess)
		{
			var items = result.AllItems;
			var equippedItems = result.EquippedItems;
			float fadeTime = (float)UFE.config.gameGUI.screenFadeDuration;
			UFE.gameMode = GameMode.VersusMode;
			UFE.SetCPU(1, false);
			UFE.SetCPU(2, true);
			List<UFE3D.CharacterInfo> characters = new List<UFE3D.CharacterInfo>();
			for (int i = 0; i < UFE.config.characters.Length; ++i)
			{
				characters.Add(UFE.config.characters[i]);
			}

			setupRig(equippedItems);
			
			foreach (var item in equippedItems)
			{
				if (item.Category == "Hero")
				{
					CharacterName = item.DisplayName;
				}
			}
			for (int i = 0; i < characters.Count; ++i)
			{
				if (characters[i].characterName == CharacterName)
				{
					UFE.SetPlayer(1, characters[i]);
				}
			}
			System.Random randomCharacter = new System.Random();
			int characterId = randomCharacter.Next(0, characters.Count - 1);
			UFE.SetPlayer(2, characters[characterId]);
			System.Random randomStage = new System.Random();
			int stageId = randomStage.Next(1, UFE.config.stages.Length - 1);
			UFE.SetStage(UFE.config.stages[stageId]);
			UFE.StartLoadingBattleScreen();
		}
	}

	private void OnGetInventoryAndGoToStoryModeScreen(CBSGetInventoryResult result)
	{
		popupViewer.HideLoadingPopup();
		if (result.IsSuccess)
		{
			var items = result.AllItems;
			var equippedItems = result.EquippedItems;
			float fadeTime = (float)UFE.config.gameGUI.screenFadeDuration;
			UFE3D.CharacterInfo character = UFE.config.characters[0];
			List<UFE3D.CharacterInfo> characters = new List<UFE3D.CharacterInfo>();
			for (int i = 0; i < UFE.config.characters.Length; ++i)
			{
				characters.Add(UFE.config.characters[i]);
			}
			setupRig(equippedItems);
			
			foreach (var item in equippedItems)
			{
				if (item.Category == "Hero")
				{
					CharacterName = item.DisplayName;
				}
			}
			for (int i = 0; i < characters.Count; ++i)
			{
				if (characters[i].characterName == CharacterName)
                {
					character = characters[i];
				}
			}
			System.Random randomStage = new System.Random();
			int stageId = randomStage.Next(1, UFE.config.stages.Length - 1);
			UFE.SetStage(UFE.config.stages[stageId]);
			UFE.StartStoryModeWithCharacter(character, fadeTime);
		}
	}

	private void setupRig(List<CBSInventoryItem> items)
    {
		var cf = GameObject.Find("CF2-UFE-Configurable-Rig(Clone)");
		Transform cfbgroup = null;
		Transform cfcanvas = null;
		GameObject cfpanel = null;
		if (cf == null) return;
		cfcanvas = cf.transform.Find("CF2-Canvas");
		if (cfcanvas == null) return;
		cfpanel = cfcanvas.Find("CF2-Panel").gameObject;
		if (cfpanel == null) return;
		cfbgroup = cfpanel.transform.Find("6-Button-Group");
		if (cfbgroup == null) return;
		cfbgroup.Find("ulti_megablast").gameObject.SetActive(false);
		cfbgroup.Find("ulti_shadowhelp").gameObject.SetActive(false);
		cfbgroup.Find("skill_focus").gameObject.SetActive(false);
		cfbgroup.Find("skill_blueblast").gameObject.SetActive(false);
		cfbgroup.Find("skill_helperninja").gameObject.SetActive(false);
		cfbgroup.Find("skill_fireball").gameObject.SetActive(false);
		cfbgroup.Find("skill_megapunch").gameObject.SetActive(false);
		cfbgroup.Find("ulti_berserk").gameObject.SetActive(false);
		var abilityCounter = 0;
		var ultiCounter = 0;
		Dictionary<int, Dictionary<string, int>> abilityCoords = new Dictionary<int, Dictionary<string, int>>()
		{
			{ 1, new Dictionary<string, int>() { 
					{ "x", 800 },
					{ "y", 74 }
				} 
			},
			{ 2, new Dictionary<string, int>() {
					{ "x", 700 },
					{ "y", 74 }
				}
			}
		};
		Dictionary<int, Dictionary<string, int>> ultiCoords = new Dictionary<int, Dictionary<string, int>>()
		{
			{ 1, new Dictionary<string, int>() {
					{ "x", 1090 },
					{ "y", 340 }
				}
			},
		};
		foreach (var item in items)
		{
			if (item.Category == "Ability")
            {
				abilityCounter++;
				if (abilityCounter > abilityCoords.Count) continue;
				var ability = cfbgroup.Find(item.ItemID).gameObject;
				var lowKick = cfbgroup.Find("low_kick").gameObject;
				RectTransform rectTransform = lowKick.GetComponent<RectTransform>();
				ability.transform.localPosition = new Vector3(lowKick.transform.localPosition.x - rectTransform.rect.width * abilityCounter - 20 * abilityCounter, lowKick.transform.localPosition.y, 25);
				Debug.Log("set ability");
				ability.SetActive(true);
			}
			if (item.Category == "Ulti")
            {
				ultiCounter++;
				if (ultiCounter > abilityCoords.Count) continue;
				var ulti = cfbgroup.Find(item.ItemID).gameObject;
				//ulti.transform.position = new Vector3(Screen.width - 80, ultiCoords[ultiCounter]["y"], 25);
				Debug.Log("set ulti");
				ulti.SetActive(true);
			}
		}
	}

	public override void OnShow (){
		base.OnShow ();
		this.HighlightOption(this.FindFirstSelectable());

		if (this.music != null){
			UFE.DelayLocalAction(delegate(){UFE.PlayMusic(this.music);}, this.delayBeforePlayingMusic);
		}
		
		if (this.stopPreviousSoundEffectsOnLoad){
			UFE.StopSounds();
		}
		
		if (this.onLoadSound != null){
			UFE.DelayLocalAction(delegate(){UFE.PlaySound(this.onLoadSound);}, this.delayBeforePlayingMusic);
		}

		if (buttonNetwork != null) {
			buttonNetwork.interactable = UFE.isNetworkAddonInstalled || UFE.isBluetoothAddonInstalled;
		}

		if (buttonBluetooth != null){
            buttonBluetooth.interactable = UFE.isBluetoothAddonInstalled;
        }
	}
	#endregion
}
