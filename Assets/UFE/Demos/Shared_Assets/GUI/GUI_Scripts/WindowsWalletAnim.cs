using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Satoshi.Animation;
using UnityEngine;

public class WindowsWalletAnim : MonoBehaviour
{
    public void Open()
    {
        AnimationSettingsConfigurator.instance.InAnimation(transform);
        // transform.LeanScale(Vector2.one, 0.6f);
    }

    public void Close()
    {
        AnimationSettingsConfigurator.instance.OutAnimation(transform);
    }
}
