using CBS;
using CBS.Models;
using CBS.UI;
using UnityEngine;

public class CheckCharacterOnLoad : MonoBehaviour
{
    private ICBSInventory Inventory { get; set; }
    private string CharacterName = "Boxer";
    private PopupViewer popupViewer;
    private void Start()
    {
        SetCharacterFromEquip();
    }

    public void SetCharacterFromEquip()
    {
        popupViewer = new PopupViewer();
        popupViewer.ShowLoadingPopup();
        Inventory = CBSModule.Get<CBSInventoryModule>();
        Inventory.GetInventory(OnGetInventory);
    }

    private void OnGetInventory(CBSGetInventoryResult result)
    {
        popupViewer.HideLoadingPopup();
        if (result.IsSuccess)
        {
            var items = result.AllItems;
            var equippedItems = result.EquippedItems;

            
            // Находим объект CanvasMainMenu на сцене
            GameObject canvasMainMenu = GameObject.Find("CanvasMainMenu");

            // Проверяем, что объект CanvasMainMenu был найден
            if (canvasMainMenu != null)
            {
                // Находим объект Back внутри CanvasMainMenu
                Transform back = canvasMainMenu.transform.Find("Back");

                // Проверяем, что объект Back был найден
                if (back != null)
                {
                    // Находим объект Character внутри Back
                    GameObject character = back.Find("Character").gameObject;

                    // Проверяем, что объект Character был найден
                    if (character != null)
                    {
                        if(!character.activeSelf)
                        {
                            character.SetActive(true);
                        }
                        var heroUnequipeed = character.transform.Find(CharacterName).gameObject;
                        heroUnequipeed.SetActive(false);

                        foreach (var item in equippedItems)
                        {
                            if (item.Category == "Hero")
                            {
                                CharacterName = item.DisplayName;
                            }
                        }
                        Debug.Log(CharacterName);
                        var heroEquipped = character.transform.Find(CharacterName).gameObject;
                        heroEquipped.SetActive(true);
                    }
                    else
                    {
                        Debug.LogError("Объект Character не найден внутри Back или уже активен!");
                    }
                }
                else
                {
                    Debug.LogError("Объект Back не найден в CanvasMainMenu!");
                }
            }
            else
            {
                Debug.LogError("CanvasMainMenu не найден на сцене!");
            }
        }
    }
}