using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using FPLibrary;
using UFE3D;

public class ButtonGUISoundSFC : MonoBehaviour
{
    #region public instance properties
    public AudioClip SelectedSound;
    #endregion

    public void ButtonSoundGUI()
    {
        if (SelectedSound != null)
        {
            UFE.PlaySound(SelectedSound);
        }
        
    }
}
