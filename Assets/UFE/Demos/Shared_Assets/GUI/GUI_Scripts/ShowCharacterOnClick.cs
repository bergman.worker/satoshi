using UnityEngine;

public class ShowCharacterOnClick : MonoBehaviour
{
    private void Start()
    {
        // Подписываемся на событие нажатия кнопки
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(ShowCharacter);
    }

    private void ShowCharacter()
    {
        // Находим объект CanvasMainMenu на сцене
        GameObject canvasMainMenu = GameObject.Find("CanvasMainMenu");

        // Проверяем, что объект CanvasMainMenu был найден
        if (canvasMainMenu != null)
        {
            // Находим объект Back внутри CanvasMainMenu
            Transform back = canvasMainMenu.transform.Find("Back");

            // Проверяем, что объект Back был найден
            if (back != null)
            {
                // Находим объект Character внутри Back
                Transform character = back.Find("Character");

                // Проверяем, что объект Character был найден
                if (character != null)
                {
                    // Делаем объект Character активным
                    character.gameObject.SetActive(true);
                }
                else
                {
                    Debug.LogError("Объект Character не найден внутри Back!");
                }

                // Находим объект Fog внутри Back
                // Transform fog = back.Find("Fog");

                // // Проверяем, что объект Fog был найден
                // if (fog != null)
                // {
                //     // Делаем объект Fog активным
                //     fog.gameObject.SetActive(true);
                // }
                // else
                // {
                //     Debug.LogError("Объект Fog не найден внутри Back!");
                // }
            }
            else
            {
                Debug.LogError("Объект Back не найден внутри CanvasMainMenu!");
            }
        }
        else
        {
            Debug.LogError("CanvasMainMenu не найден на сцене!");
        }
    }
}
