using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class LoginGUIButtonSound : MonoBehaviour
{
    public AudioSource GUIaudioLogin; 
    public AudioClip LoginClickSound;
    
    public void LoginSoundButtonClick()
    {
        GUIaudioLogin.PlayOneShot(LoginClickSound);
    }
}