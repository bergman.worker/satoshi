using System.Collections;
using System.Collections.Generic;
using CBS;
using CBS.Models;
using CBS.UI;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class WalletPanel : MonoBehaviour
{
    private PopupViewer popupViewer;
    private IWeb3 CBSWeb3 { get; set; }
    private IProfile CBSProfile { get; set; }

    private String walletAddressKey = "wallet_address";
    private String walletPrivateKeyKey = "wallet_private_key";

    private int saveProfileDataCounter = 0;

    [SerializeField]
    //public Text textGameBalance;
    public TMP_Text textGameBalance;
    [SerializeField]
    public TextMeshProUGUI textAddressSFC;
    [SerializeField]
    //public Text textBalanceSFC;
    public TMP_Text textBalanceSFC;
    [SerializeField]
    public TextMeshProUGUI textAddressBNB;
    [SerializeField]
    //public Text textBalanceBNB;
    public TMP_Text textBalanceBNB;
    [SerializeField]
    //public InputField enterQuantityPushToWallet;
    public TMP_InputField enterQuantityPushToWallet; 
    [SerializeField]
    //public InputField enterQuantityPushToGameBalance;
    public TMP_InputField enterQuantityPushToGameBalance;
    public GameObject confirmToWallet;
    public GameObject confirmToGameBalance;

    void Start()
    {
        Debug.Log(1);
        popupViewer = new PopupViewer();
        popupViewer.ShowLoadingPopup();
        CBSWeb3 = CBSModule.Get<CBSWeb3Module>();
        CBSProfile = CBSModule.Get<CBSProfileModule>();
    }

    private void OnEnable()
    {
        saveProfileDataCounter = 0;
        popupViewer = new PopupViewer();
        popupViewer.ShowLoadingPopup();
        CBSWeb3 = CBSModule.Get<CBSWeb3Module>();
        CBSProfile = CBSModule.Get<CBSProfileModule>();
        CBSWeb3.GetWalletData(CBSProfile.ProfileID, OnGetWalletData);
    }

    public void copyWalletAddressToClipboard()
    {
        GUIUtility.systemCopyBuffer = textAddressSFC.text;
        new PopupViewer().ShowSimplePopup(new PopupRequest
        {
            Title = "Success",
            Body = "Wallet address copied to clipboard."
        });
    }

    private void OnGetWalletData(CBSGetWalletDataResult result)
    {
        popupViewer.HideLoadingPopup();
        if (result.IsSuccess)
        {
            textGameBalance.text = result.gameBalance + " LGM";
            textAddressSFC.text = result.walletAddress;
            textBalanceSFC.text = result.walletTokenBalance + " LGM";
            textAddressBNB.text = result.walletAddress;
            textBalanceBNB.text = result.walletBNBBalance + " BNB";
        }
        else
        {
            Debug.Log(result.Error.Message);
        }
    }

    public void LogoutWallet()
    {
        popupViewer.ShowLoadingPopup();
        CBSProfile.SaveProfileData(walletAddressKey, "", OnSafeProfileData);
        CBSProfile.SaveProfileData(walletPrivateKeyKey, "", OnSafeProfileData);
    }

    private void OnSafeProfileData(CBSBaseResult result)
    {
        if (result.IsSuccess)
        {
            saveProfileDataCounter++;
            if (saveProfileDataCounter == 2)
            {
                popupViewer.HideLoadingPopup();
                saveProfileDataCounter = 0;
                // transform.LeanScale(Vector2.zero, 0.8f).setEaseInBack().setOnComplete(() =>
                // {
                //     gameObject.SetActive(false);
                // });
            }
        }
        else
        {
            Debug.Log(result.Error.Message);
        }
    }

    public void PushToWallet()
    {
        popupViewer.ShowLoadingPopup();
        // CBSProfile.SaveProfileData(walletAddressKey, "", OnSafeProfileData);
        // CBSProfile.SaveProfileData(walletPrivateKeyKey, "", OnSafeProfileData);
        int amount = Int32.Parse(enterQuantityPushToWallet.text);
        CBSWeb3.PushToWallet(CBSProfile.ProfileID, amount, OnPushToWallet);
    }

    public void PushToGameBalance()
    {
        popupViewer.ShowLoadingPopup();
        // CBSProfile.SaveProfileData(walletAddressKey, "", OnSafeProfileData);
        // CBSProfile.SaveProfileData(walletPrivateKeyKey, "", OnSafeProfileData);
        int amount = Int32.Parse(enterQuantityPushToGameBalance.text);
        CBSWeb3.PushToGameBalance(CBSProfile.ProfileID, amount, OnPushToGameBalance);
    }

    private void OnPushToWallet(CBSPushTokenResult result)
    {
        popupViewer.HideLoadingPopup();
        if (result.IsSuccess)
        {
            confirmToWallet.SetActive(false);
            OnEnable();
        }
        else
        {
            confirmToWallet.SetActive(false);
            new PopupViewer().ShowSimplePopup(new PopupRequest
            {
                Title = "Transaction error",
                Body = result.Error.Message
            });
        }
    }

    private void OnPushToGameBalance(CBSPushTokenResult result)
    {
        popupViewer.HideLoadingPopup();
        if (result.IsSuccess)
        {
            confirmToGameBalance.SetActive(false);
            OnEnable();
        }
        else
        {
            confirmToGameBalance.SetActive(false);
            new PopupViewer().ShowSimplePopup(new PopupRequest
            {
                Title = "Transaction error",
                Body = result.Error.Message
            });
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
