using System.Collections;
using System.Collections.Generic;
using CBS;
using CBS.Models;
using CBS.UI;
using UnityEngine;
using TMPro;

public class WalletCreate : MonoBehaviour
{
    private PopupViewer popupViewer;
    private IWeb3 CBSWeb3 { get; set; }
    private IProfile CBSProfile { get; set; }
    private string mnemonic;

    [SerializeField]
    public TMPro.TextMeshProUGUI textMnemonic;

    public GameObject newWalletWindow;
    public GameObject createImportGroup;
    public GameObject walletPanelWindow;
    void Start()
    {
        popupViewer = new PopupViewer();
        popupViewer.ShowLoadingPopup();
        CBSWeb3 = CBSModule.Get<CBSWeb3Module>();
        CBSProfile = CBSModule.Get<CBSProfileModule>();
        CBSWeb3.GetMnemonic(OnGetMnemonic);
    }

    private void OnGetMnemonic(CBSGetMnemonicResult result)
    {
        popupViewer.HideLoadingPopup();
        if (result.IsSuccess)
        {
            mnemonic = result.mnemonic;
            textMnemonic.text = result.mnemonic;
            Debug.Log(mnemonic);
        }
        else
        {
            Debug.Log(result.Error.Message);
        }
    }

    public void copyMnemonicToClipboard()
    {
        GUIUtility.systemCopyBuffer = textMnemonic.text;
        new PopupViewer().ShowSimplePopup(new PopupRequest
        {
            Title = "Success",
            Body = "Wallet mnemonic copied to clipboard."
        });
    }

    public void CreateWallet()
    {
        popupViewer.ShowLoadingPopup();
        CBSWeb3.CreateWallet(CBSProfile.ProfileID, mnemonic, OnCreateWallet);
    }

    private void OnCreateWallet(CBSCreateWalletResult result)
    {
        popupViewer.HideLoadingPopup();
        if (result.IsSuccess)
        {
            newWalletWindow.SetActive(false);
            createImportGroup.SetActive(false);
            Debug.Log(result.privateKey);
            walletPanelWindow.SetActive(true);
            var windowsWalletAnim = walletPanelWindow.GetComponent<WindowsWalletAnim>();
            windowsWalletAnim.Open();
        }
        else
        {
            Debug.Log(result.Error.Message);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
