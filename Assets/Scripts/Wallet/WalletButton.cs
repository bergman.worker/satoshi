using System;
using System.Collections;
using System.Collections.Generic;
using CBS;
using CBS.Models;
using CBS.UI;
using UnityEngine;

public class WalletButton : MonoBehaviour
{
    private IProfile CBSProfile { get; set; }
    private CBSCurrencyModule CBSCurrencyModule { get; set; }
    private PopupViewer popupViewer;
    private String walletAddressKey = "wallet_address";
    private String walletPrivateKeyKey = "wallet_private_key";
    public GameObject createImportGroup;
    public GameObject createImportWindow;
    public GameObject walletWindow;
    public void Open()
    {
        //��������� ������� �������� � ���������� �����
        CBSProfile = CBSModule.Get<CBSProfileModule>();
        var profileDataKeys = new string[]
            {
                walletAddressKey,
                walletPrivateKeyKey
            };
        popupViewer = new PopupViewer();
        popupViewer.ShowLoadingPopup();
        CBSProfile.GetProfileData(profileDataKeys, OnGetProfileInfo);
    }

    private void OnGetProfileInfo(CBSGetProfileDataResult result)
    {
        popupViewer.HideLoadingPopup();
        if (result.IsSuccess)
        {
            var dataDictionaty = result.Data;
            String walletAddress, walletPrivateKey;
            try
            {
                walletAddress = dataDictionaty[walletAddressKey].Value;
                walletPrivateKey = dataDictionaty[walletPrivateKeyKey].Value;
                if (walletAddress.Length == 0 || walletPrivateKey.Length == 0)
                {
                    createImportGroup.SetActive(true);
                    createImportWindow.SetActive(true);
                    //�������� ���, ��������� ���� �������/�������������
                }
                else
                {
                    walletWindow.SetActive(true);
                    var windowsWalletAnim = walletWindow.GetComponent<WindowsWalletAnim>();
                    windowsWalletAnim.Open();
                    //������� ����, ��������� ���� WalletPanel
                }
            }
            catch (Exception e)
            {
                createImportGroup.SetActive(true);
                createImportWindow.SetActive(true);
                //�������� ���, ��������� ���� �������/�������������
            }
        }
        else
        {
            Debug.Log(result.Error.Message);
        }
    }
}
