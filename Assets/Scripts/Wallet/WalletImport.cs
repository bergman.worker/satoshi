using System.Collections;
using System.Collections.Generic;
using CBS;
using CBS.Models;
using CBS.UI;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WalletImport : MonoBehaviour
{
    private PopupViewer popupViewer;
    private IWeb3 CBSWeb3 { get; set; }
    private IProfile CBSProfile { get; set; }

    [SerializeField]
    //public InputField inputFieldMnemonic;
    public TMP_InputField inputFieldMnemonic;
    [SerializeField]
    //public InputField inputFieldPrivateKey;
    public TMP_InputField inputFieldPrivateKey;

    public GameObject importWalletFromMnemonicWindow;
    public GameObject importWalletFromPrivateKeyWindow;
    public GameObject createImportGroup;
    public GameObject walletPanelWindow;
    void Start()
    {
        CBSWeb3 = CBSModule.Get<CBSWeb3Module>();
        CBSProfile = CBSModule.Get<CBSProfileModule>();
        popupViewer = new PopupViewer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ImportWalletFromMnemonic()
    {
        popupViewer.ShowLoadingPopup();
        var mnemonic = inputFieldMnemonic.text;
        CBSWeb3.ImportWalletFromMnemonic(CBSProfile.ProfileID, mnemonic, OnImportWallet);
    }

    public void ImportWalletFromPrivateKey()
    {
        popupViewer.ShowLoadingPopup();
        var privateKey = inputFieldPrivateKey.text;
        CBSWeb3.ImportWalletFromPrivateKey(CBSProfile.ProfileID, privateKey, OnImportWallet);
    }

    private void OnImportWallet(CBSCreateWalletResult result)
    {
        popupViewer.HideLoadingPopup();
        if (result.IsSuccess)
        {
            importWalletFromMnemonicWindow.SetActive(false);
            importWalletFromPrivateKeyWindow.SetActive(false);
            createImportGroup.SetActive(false);
            Debug.Log(result.privateKey);
            walletPanelWindow.SetActive(true);
            var windowsWalletAnim = walletPanelWindow.GetComponent<WindowsWalletAnim>();
            windowsWalletAnim.Open();
        }
        else
        {
            Debug.Log(result.Error.Message);
        }
    }
}
