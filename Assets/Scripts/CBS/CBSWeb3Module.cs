using CBS.Core.Auth;
using CBS.Models;
using CBS.Playfab;
using CBS.Scriptable;
using CBS.Utils;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CBS
{
    public class CBSWeb3Module : CBSModule, IWeb3
    {

        private IFabWeb3 FabWeb3 { get; set; }

        protected override void Init()
        {
            FabWeb3 = FabExecuter.Get<FabWeb3>();
        }
        public void GetMnemonic(Action<CBSGetMnemonicResult> result)
        {
            FabWeb3.GetMnemonic(onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSGetMnemonicResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var mnemonicResult = onGet.GetResult<FunctionMnemonicResult>();

                    result?.Invoke(new CBSGetMnemonicResult
                    {
                        IsSuccess = true,
                        mnemonic = mnemonicResult.mnemonic
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSGetMnemonicResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void CreateWallet(string profileID, string mnemonic, Action<CBSCreateWalletResult> result)
        {
            FabWeb3.CreateWallet(profileID, mnemonic, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSCreateWalletResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var walletResult = onGet.GetResult<FunctionWalletResult>();

                    result?.Invoke(new CBSCreateWalletResult
                    {
                        IsSuccess = true,
                        address = walletResult.address,
                        privateKey = walletResult.privateKey
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSCreateWalletResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void ImportWalletFromMnemonic(string profileID, string mnemonic, Action<CBSCreateWalletResult> result)
        {
            FabWeb3.ImportWalletFromMnemonic(profileID, mnemonic, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSCreateWalletResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var walletResult = onGet.GetResult<FunctionWalletResult>();

                    result?.Invoke(new CBSCreateWalletResult
                    {
                        IsSuccess = true,
                        address = walletResult.address,
                        privateKey = walletResult.privateKey
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSCreateWalletResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void ImportWalletFromPrivateKey(string profileID, string privateKey, Action<CBSCreateWalletResult> result)
        {
            FabWeb3.ImportWalletFromPrivateKey(profileID, privateKey, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSCreateWalletResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var walletResult = onGet.GetResult<FunctionWalletResult>();

                    result?.Invoke(new CBSCreateWalletResult
                    {
                        IsSuccess = true,
                        address = walletResult.address,
                        privateKey = walletResult.privateKey
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSCreateWalletResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void GetWalletData(string profileID, Action<CBSGetWalletDataResult> result)
        {
            FabWeb3.GetWalletData(profileID, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSGetWalletDataResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var walletDataResult = onGet.GetResult<FunctionWalletDataResult>();

                    result?.Invoke(new CBSGetWalletDataResult
                    {
                        IsSuccess = true,
                        walletAddress = walletDataResult.walletAddress,
                        gameBalance = walletDataResult.gameBalance,
                        walletBNBBalance = walletDataResult.walletBNBBalance,
                        walletTokenBalance = walletDataResult.walletTokenBalance
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSGetWalletDataResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void PushToWallet(string profileID, int amount, Action<CBSPushTokenResult> result)
        {
            FabWeb3.PushToWallet(profileID, amount, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSPushTokenResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var walletDataResult = onGet.GetResult<FunctionPushTokenResult>();

                    result?.Invoke(new CBSPushTokenResult
                    {
                        IsSuccess = true,
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSPushTokenResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void PushToGameBalance(string profileID, int amount, Action<CBSPushTokenResult> result)
        {
            FabWeb3.PushToGameBalance(profileID, amount, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSPushTokenResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var walletDataResult = onGet.GetResult<FunctionPushTokenResult>();

                    result?.Invoke(new CBSPushTokenResult
                    {
                        IsSuccess = true,
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSPushTokenResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }
    }
}
