
using PlayFab;

namespace CBS.Models
{
    public class FunctionBuyItemFromPlayerStoreRequest : FunctionBaseRequest
    {
        public string tradeId;
        public PlayFabAuthenticationContext ProfileAuthContext;
    }
}