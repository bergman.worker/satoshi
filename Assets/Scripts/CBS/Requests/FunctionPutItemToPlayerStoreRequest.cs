
using PlayFab;

namespace CBS.Models
{
    public class FunctionPutItemToPlayerStoreRequest : FunctionBaseRequest
    {
        public string itemInstanceId;
        public string itemId;
        public int price;
        public PlayFabAuthenticationContext ProfileAuthContext;
    }
}