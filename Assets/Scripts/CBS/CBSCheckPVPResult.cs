using PlayFab.ClientModels;
using System.Collections.Generic;

namespace CBS.Models
{
    public class CBSCheckPVPResult : CBSBaseResult
    {
        public string pvpId;
        public string CreatorId;
        public string OpponentId;
        public string CreatorName;
        public string OpponentName;
        public int bet;
        public string Status;
    }
}