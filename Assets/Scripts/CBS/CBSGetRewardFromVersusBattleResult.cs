using PlayFab.ClientModels;
using System.Collections.Generic;

namespace CBS.Models
{
    public class CBSGetRewardFromVersusBattleResult : CBSBaseResult
    {
        public int amount;
        public int exp;
    }
}