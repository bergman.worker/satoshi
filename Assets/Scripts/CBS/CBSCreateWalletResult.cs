using PlayFab.ClientModels;
using System.Collections.Generic;

namespace CBS.Models
{
    public class CBSCreateWalletResult : CBSBaseResult
    {
        public string address;
        public string privateKey;
    }
}