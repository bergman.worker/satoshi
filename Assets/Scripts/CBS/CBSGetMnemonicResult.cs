using PlayFab.ClientModels;
using System.Collections.Generic;

namespace CBS.Models
{
    public class CBSGetMnemonicResult : CBSBaseResult
    {
        public string mnemonic;
    }
}