using CBS.Models;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections.Generic;

namespace CBS
{
    public interface IWeb3
    {
        void GetMnemonic(Action<CBSGetMnemonicResult> result);
        void CreateWallet(string profileID, string mnemonic, Action<CBSCreateWalletResult> result);
        void ImportWalletFromMnemonic(string profileID, string mnemonic, Action<CBSCreateWalletResult> result);
        void ImportWalletFromPrivateKey(string profileID, string privateKey, Action<CBSCreateWalletResult> result);
        void GetWalletData(string profileID, Action<CBSGetWalletDataResult> result);
        void PushToWallet(string profileID, int amount, Action<CBSPushTokenResult> result);
        void PushToGameBalance(string profileID, int amount, Action<CBSPushTokenResult> result);
    }
}
