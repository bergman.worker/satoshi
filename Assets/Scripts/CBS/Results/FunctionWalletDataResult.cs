
using System.Collections.Generic;

namespace CBS.Models
{
    public class FunctionWalletDataResult
    {
        public string gameBalance;
        public string walletTokenBalance;
        public string walletBNBBalance;
        public string walletAddress;
    }
}
