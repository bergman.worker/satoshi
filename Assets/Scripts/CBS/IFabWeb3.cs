using CBS.Models;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.CloudScriptModels;
using System;

namespace CBS.Playfab
{
    public interface IFabWeb3
    {
        void GetMnemonic(Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
        void CreateWallet(string profileID, string mnemonic, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
        void ImportWalletFromMnemonic(string profileID, string mnemonic, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
        void ImportWalletFromPrivateKey(string profileID, string privateKey, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
        void GetWalletData(string profileID, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
        void PushToWallet(string profileID, int amount, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
        void PushToGameBalance(string profileID, int amount, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
    }
}
