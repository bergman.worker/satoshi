using PlayFab.ClientModels;
using System.Collections.Generic;

namespace CBS.Models
{
    public class CBSGetWalletDataResult : CBSBaseResult
    {
        public string gameBalance;
        public string walletTokenBalance;
        public string walletBNBBalance;
        public string walletAddress;
    }
}