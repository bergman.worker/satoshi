using CBS.Models;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.CloudScriptModels;
using System;

namespace CBS.Playfab
{
    public class FabWeb3 : FabExecuter, IFabWeb3
    {
        public void GetMnemonic(Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.GetMnemonicMethod,
                FunctionParameter = new FunctionBaseRequest{}
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void CreateWallet(string profileID, string mnemonic, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.CreateWalletMethod,
                FunctionParameter = new FunctionMnemonicRequest
                {
                    ProfileID = profileID,
                    mnemonic = mnemonic
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void ImportWalletFromMnemonic(string profileID, string mnemonic, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.ImportWalletFromMnemonicMethod,
                FunctionParameter = new FunctionMnemonicRequest
                {
                    ProfileID = profileID,
                    mnemonic = mnemonic
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void ImportWalletFromPrivateKey(string profileID, string privateKey, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.ImportWalletFromPrivateKeyMethod,
                FunctionParameter = new FunctionPrivateKeyRequest
                {
                    ProfileID = profileID,
                    privateKey = privateKey
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void GetWalletData(string profileID, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.GetWalletDataMethod,
                FunctionParameter = new FunctionBaseRequest
                {
                    ProfileID = profileID
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void PushToWallet(string profileID, int amount, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.PushToWalletMethod,
                FunctionParameter = new FunctionPushTokenRequest
                {
                    ProfileID = profileID,
                    amount = amount
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void PushToGameBalance(string profileID, int amount, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.PushToGameBalanceMethod,
                FunctionParameter = new FunctionPushTokenRequest
                {
                    ProfileID = profileID,
                    amount = amount
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }
    }
}