using System.Collections;
using System.Collections.Generic;
using CBS;
using CBS.Models;
using UnityEngine;

public class ListenFriendRequest : MonoBehaviour
{
    private IMatchmaking Matchmaking { get; set; }
    private IProfile Profile { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        Matchmaking = CBSModule.Get<CBSMatchmakingModule>();
        Profile = CBSModule.Get<CBSProfileModule>();
        //Matchmaking.OnStatusChanged += OnStatusChanged;
        var queueName = Profile.ProfileID;

        var findRequest = new CBSFindMatchRequest
        {
            QueueName = queueName
        };

        //Matchmaking.FindMatch(findRequest, OnStartFindMatch);
    }

    private void OnStartFindMatch(CBSFindMatchResult result)
    {
        if (result.IsSuccess)
        {
            Debug.Log("Start search opponent with ticket id " + result.TicketID);
        }
        else
        {
            Debug.Log(result.Error.Message);
        }
    }

    private void OnStatusChanged(MatchmakingStatus status)
    {
        Debug.Log("Status of ticket was changed to " + status);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
