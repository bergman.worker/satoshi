using UnityEngine;
using UnityEngine.UI;

public class SwitchButtonImages : MonoBehaviour
{
    public Button button1;
    public Button button2;
    public Sprite activeImage; // Изображение для активного состояния
    public Sprite inactiveImage; // Изображение для неактивного состояния

    // Вызывается при старте для установки начальных изображений
    void Start()
    {
        button1.image.sprite = activeImage; // Первая кнопка активна изначально
        button2.image.sprite = inactiveImage; // Вторая кнопка неактивна изначально
    }

    // Метод для активации первой кнопки
    public void ActivateButton1()
    {
        if (button2.image.sprite == activeImage) // Проверка, что вторая кнопка активна
        {
            button1.image.sprite = activeImage;
            button2.image.sprite = inactiveImage;
        }
    }

    // Метод для активации второй кнопки
    public void ActivateButton2()
    {
        if (button1.image.sprite == activeImage) // Проверка, что первая кнопка активна
        {
            button2.image.sprite = activeImage;
            button1.image.sprite = inactiveImage;
        }
    }
}
