﻿using CBS.Models;
using PlayFab;
using PlayFab.CloudScriptModels;
using PlayFab.MultiplayerModels;
using System;

namespace CBS.Playfab
{
    public class FabMatchmaking : FabExecuter, IFabMatchmaking
    {
        public void GetMatchmakingList(Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.GetMatchmakingListMethod,
                FunctionParameter = new FunctionMatchmakingQueueRequest()
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void CreateTicket(string queueName, int waitTime, string entityID, MatchmakingPlayerAttributes attributes, Action<CreateMatchmakingTicketResult> onCreate, Action<PlayFabError> onFailed)
        {
            var request = new CreateMatchmakingTicketRequest
            {
                Creator = new MatchmakingPlayer
                {
                    Attributes = attributes,
                    Entity = new PlayFab.MultiplayerModels.EntityKey
                    {
                        Id = entityID,
                        Type = CBSConstants.EntityPlayerType
                    }
                },
                QueueName = queueName,
                GiveUpAfterSeconds = waitTime
            };
            PlayFabMultiplayerAPI.CreateMatchmakingTicket(request, onCreate, onFailed);
        }

        public void GetMatchmakingTicket(string queueName, string ticketID, Action<GetMatchmakingTicketResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new GetMatchmakingTicketRequest
            {
                QueueName = queueName,
                TicketId = ticketID
            };
            PlayFabMultiplayerAPI.GetMatchmakingTicket(request, onGet, onFailed);
        }

        public void GetQueue(string queueName, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.GetMatchmakingQueueMethod,
                FunctionParameter = new FunctionMatchmakingQueueRequest
                {
                    Queue = queueName
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void GetMatch(string queueName, string matchID, Action<GetMatchResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new GetMatchRequest
            {
                QueueName = queueName,
                MatchId = matchID,
                ReturnMemberAttributes = true
            };
            PlayFabMultiplayerAPI.GetMatch(request, onGet, onFailed);
        }

        public void CancelMatchForPlayer(string queueName, string entityID, Action<CancelAllMatchmakingTicketsForPlayerResult> onCancel, Action<PlayFabError> onFailed)
        {
            var request = new CancelAllMatchmakingTicketsForPlayerRequest
            {
                QueueName = queueName,
                Entity = new PlayFab.MultiplayerModels.EntityKey
                {
                    Id = entityID,
                    Type = CBSConstants.EntityPlayerType
                }
            };
            PlayFabMultiplayerAPI.CancelAllMatchmakingTicketsForPlayer(request, onCancel, onFailed);
        }

        public void GetMatch(string queueName, string matchID, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.GetMatchMethod,
                FunctionParameter = new FunctionMatchRequest
                {
                    Queue = queueName,
                    MatchID = matchID
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void GetRewardsFromVersusBattle(string profileID, bool isWinner,  int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.GetRewardsFromVersusBattleMethod,
                FunctionParameter = new FunctionGetRewardFromVersusBattleRequest
                {
                    ProfileID = profileID,
                    isWinner = isWinner,
                    bet = bet
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void CreatePVP(string ProfileId, string OpponentId, int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.CreatePVPMethod,
                FunctionParameter = new FunctionCreatePVPRequest
                {
                    ProfileID = ProfileId,
                    OpponentId = OpponentId,
                    bet = bet
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void StartVersusBattle(string ProfileId, int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.StartVersusBattleMethod,
                FunctionParameter = new FunctionStartVersusBattleRequest
                {
                    ProfileID = ProfileId,
                    bet = bet
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void StartCasinoBattle(string ProfileId, int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.StartCasinoBattleMethod,
                FunctionParameter = new FunctionStartVersusBattleRequest
                {
                    ProfileID = ProfileId,
                    bet = bet
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void GetRewardsFromCasinoBattle(string profileID, bool isWinner, int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.GetRewardsFromCasinoBattleMethod,
                FunctionParameter = new FunctionGetRewardFromVersusBattleRequest
                {
                    ProfileID = profileID,
                    isWinner = isWinner,
                    bet = bet
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void CheckPVP(string ProfileId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.CheckPVPMethod,
                FunctionParameter = new FunctionBaseRequest
                {
                    ProfileID = ProfileId
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void AcceptPVP(string pvpId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.AcceptPVPMethod,
                FunctionParameter = new FunctionAcceptPVPRequest
                {
                    pvpId = pvpId
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void DeclinePVP(string pvpId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.DeclinePVPMethod,
                FunctionParameter = new FunctionDeclinePVPRequest
                {
                    pvpId = pvpId
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void CancelPVP(string pvpId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.CancelPVPMethod,
                FunctionParameter = new FunctionDeclinePVPRequest
                {
                    pvpId = pvpId
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void StartPVP(string pvpId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.StartPVPMethod,
                FunctionParameter = new FunctionStartPVPRequest
                {
                    pvpId = pvpId
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

        public void FinishPVP(string pvpId, string winnerId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.FinishPVPMethod,
                FunctionParameter = new FunctionFinishPVPRequest
                {
                    pvpId = pvpId,
                    winnerId = winnerId
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onGet, onFailed);
        }

#if UNITY_EDITOR
        public void UpdateMatchmakingQueue(MatchmakingQueueConfig queue, Action<ExecuteFunctionResult> onUpdate, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.UpdateMatchmakingQueueMethod,
                FunctionParameter = new FunctionMatchmakingQueueRequest
                {
                    Queue = queue.ToJson()
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onUpdate, onFailed);
        }

        public void RemoveMatchmakingQueue(string queueName, Action<ExecuteFunctionResult> onRemove, Action<PlayFabError> onFailed)
        {
            var request = new ExecuteFunctionRequest
            {
                FunctionName = AzureFunctions.RemoveMatchmakingQueueMethod,
                FunctionParameter = new FunctionMatchmakingQueueRequest
                {
                    Queue = queueName
                }
            };
            PlayFabCloudScriptAPI.ExecuteFunction(request, onRemove, onFailed);
        }

        
#endif
    }
}
