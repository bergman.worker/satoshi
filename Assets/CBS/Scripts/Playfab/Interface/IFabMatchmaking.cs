﻿using CBS.Models;
using PlayFab;
using PlayFab.CloudScriptModels;
using PlayFab.MultiplayerModels;
using System;

namespace CBS.Playfab
{
    public interface IFabMatchmaking
    {
        void GetMatchmakingList(Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void CreateTicket(string queueName, int waitTime, string entityID, MatchmakingPlayerAttributes atributes, Action<CreateMatchmakingTicketResult> onCreate, Action<PlayFabError> onFailed);

        void GetMatchmakingTicket(string queueName, string ticketID, Action<GetMatchmakingTicketResult> onGet, Action<PlayFabError> onFailed);

        void GetQueue(string queueName, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void GetMatch(string queueName, string matchID, Action<GetMatchResult> onGet, Action<PlayFabError> onFailed);

        void CancelMatchForPlayer(string queueName, string entityID, Action<CancelAllMatchmakingTicketsForPlayerResult> onCancel, Action<PlayFabError> onFailed);

        void GetMatch(string queueName, string matchID, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void GetRewardsFromVersusBattle(string profileID, bool isWinner, int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void CreatePVP(string ProfileId, string OpponentId, int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void CheckPVP(string ProfileId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void AcceptPVP(string pvpId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void DeclinePVP(string pvpId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void CancelPVP(string pvpId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void StartPVP(string pvpId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
        void FinishPVP(string pvpId, string winnerId, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);

        void StartVersusBattle(string ProfileId, int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
        void StartCasinoBattle(string ProfileId, int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
        void GetRewardsFromCasinoBattle(string profileID, bool isWinner, int bet, Action<ExecuteFunctionResult> onGet, Action<PlayFabError> onFailed);
#if UNITY_EDITOR
        void UpdateMatchmakingQueue(MatchmakingQueueConfig queue, Action<ExecuteFunctionResult> onUpdate, Action<PlayFabError> onFailed);

        void RemoveMatchmakingQueue(string queueName, Action<ExecuteFunctionResult> onRemove, Action<PlayFabError> onFailed);
#endif
    }
}
