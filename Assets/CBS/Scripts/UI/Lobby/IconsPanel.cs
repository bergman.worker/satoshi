﻿using CBS.Scriptable;
using DG.Tweening;
using Satoshi.Animation;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CBS.UI
{
    public class IconsPanel : MonoBehaviour
    {
        [SerializeField]
        private string GameScene;

        public void ShowStore()
        {
            var prefabs = CBSScriptable.Get<StorePrefabs>();
            var storePrefab = prefabs.StoreWindows;
            //UIView.ShowWindow(storePrefab);
             // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(storePrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowInvertory()
        {
            var prefabs = CBSScriptable.Get<InventoryPrefabs>();
            var invertoryPrefab = prefabs.Inventory;
            //UIView.ShowWindow(invertoryPrefab);
             // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(invertoryPrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowLootBox()
        {
            var prefabs = CBSScriptable.Get<LootboxPrefabs>();
            var lootBoxPrefab = prefabs.LootBoxes;
            //UIView.ShowWindow(lootBoxPrefab);
             // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(lootBoxPrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowChat()
        {
            var prefabs = CBSScriptable.Get<ChatPrefabs>();
            var chatPrefab = prefabs.ChatWindow;
            //UIView.ShowWindow(chatPrefab);
             // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(chatPrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowFriends()
        {
            var prefabs = CBSScriptable.Get<FriendsPrefabs>();
            var friendsPrefab = prefabs.FriendsWindow;
            //UIView.ShowWindow(friendsPrefab);
             // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(friendsPrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowClan()
        {
            var prefabs = CBSScriptable.Get<ClanPrefabs>();
            var windowPrefab = prefabs.WindowLoader;
            //UIView.ShowWindow(windowPrefab);
             // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(windowPrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowLeaderboards()
        {
            var prefabs = CBSScriptable.Get<LeaderboardPrefabs>();
            var leaderboardsPrefab = prefabs.LeaderboardsWindow;
            //UIView.ShowWindow(leaderboardsPrefab);
               // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(leaderboardsPrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowTournament()
        {

        }

        public void ShowDailyBonus()
        {
            var prefabs = CBSScriptable.Get<CalendarPrefabs>();
            var dailyBonusPrefab = prefabs.CalendarWindow;
            //UIView.ShowWindow(dailyBonusPrefab);
                // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(dailyBonusPrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowRoulette()
        {
            var prefabs = CBSScriptable.Get<RoulettePrefabs>();
            var roulettePrefab = prefabs.RouletteWindow;
            //UIView.ShowWindow(roulettePrefab);
                  // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(roulettePrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowMatchmaking()
        {
            var prefabs = CBSScriptable.Get<MatchmakingPrefabs>();
            var matchmakingPrefab = prefabs.MatchmalingWindow;
            //UIView.ShowWindow(matchmakingPrefab);
                  // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(matchmakingPrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowAchievements()
        {
            var prefabs = CBSScriptable.Get<AchievementsPrefabs>();
            var achievementsWindow = prefabs.AchievementsWindow;
            //UIView.ShowWindow(achievementsWindow);
              // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(achievementsWindow);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowDailyTasks()
        {
            var prefabs = CBSScriptable.Get<ProfileTasksPrefabs>();
            var tasksWindow = prefabs.ProfileTasksWindow;
            //UIView.ShowWindow(tasksWindow);
              // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(tasksWindow);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowForge()
        {
            var prefabs = CBSScriptable.Get<CraftPrefabs>();
            var craftWindow = prefabs.CraftWindow;
            //UIView.ShowWindow(craftWindow);
              // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(craftWindow);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowNotification()
        {
            var prefabs = CBSScriptable.Get<NotificationPrefabs>();
            var notificationWindow = prefabs.NotificationWindow;
           // UIView.ShowWindow(notificationWindow);
             // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(notificationWindow);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void ShowEvents()
        {
            var prefabs = CBSScriptable.Get<EventsPrefabs>();
            var eventsWindow = prefabs.EventWindow;
            //UIView.ShowWindow(eventsWindow);
              // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(eventsWindow);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                AnimationSettingsConfigurator.instance.InAnimation(windowObject.transform);
            }
        }

        public void LoadGame()
        {
            SceneManager.LoadScene(GameScene);
        }
    }
}
