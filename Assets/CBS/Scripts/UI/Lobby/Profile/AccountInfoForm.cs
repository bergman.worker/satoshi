﻿using System;
using CBS.Models;
using CBS.Utils;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace CBS.UI
{
    public class AccountInfoForm : MonoBehaviour
    {
        [SerializeField]
        //private Text NicknameLabel;
        private TMP_Text NicknameLabel;
        [SerializeField]
        //private Text PlayfabIDLabel;
        private TMP_Text PlayfabIDLabel;
        [SerializeField]
        //private Text RegistrationLabel;
        private TMP_Text RegistrationLabel;
        [SerializeField]
        //private Text EntityIDLabel;
        private TMP_Text EntityIDLabel;
        [SerializeField]
        private GameObject UserNamePanel;
        [SerializeField]
        private GameObject EditNamePanel;
        [SerializeField]
        //private InputField EditInput;
        private TMP_InputField EditInput;
        [SerializeField]
        //private Text GameAllLabel;
        private TMP_Text GameAllLabel;
        [SerializeField]
        //private Text WinValueLabel;
        private TMP_Text WinValueLabel;

        private IProfile CBSProfile { get; set; }
        private IAuth Auth { get; set; }

        private void Awake()
        {
            CBSProfile = CBSModule.Get<CBSProfileModule>();
            Auth = CBSModule.Get<CBSAuthModule>();
        }

        private void OnEnable()
        {
            DisplayUI();
            CBSProfile.OnDisplayNameUpdated += OnUserNameUpdated;
            CBSProfile.GetAccountInfo(OnAccountInfoGetted);
            var profileDataKeys = new string[]
            {
                "game_all",
                "game_wins",
                "game_loses",
                "game_winrate"
            };
            CBSProfile.GetProfileData(profileDataKeys, OnGetProfileInfo);
        }

        private void OnDisable()
        {
            CBSProfile.OnDisplayNameUpdated -= OnUserNameUpdated;
        }

        private void DisplayUI()
        {
            ShowNickname();
            NicknameLabel.text = CBSProfile.DisplayName;
            PlayfabIDLabel.text = CBSProfile.ProfileID;
            RegistrationLabel.text = CBSProfile.RegistrationDate;
            EntityIDLabel.text = CBSProfile.EntityID;
            EditInput.text = string.Empty;
        }

        public void ShowNickname()
        {
            UserNamePanel.SetActive(true);
            EditNamePanel.SetActive(false);
        }

        public void ShowEditName()
        {
            EditInput.text = CBSProfile.DisplayName;
            UserNamePanel.SetActive(false);
            EditNamePanel.SetActive(true);
        }

        public void OnLogout()
        {
            Auth.Logout();
        }

        public void UpdateNickname()
        {
            string newName = EditInput.text;
            // check with empty filed
            if (string.IsNullOrEmpty(newName))
            {
                new PopupViewer().ShowSimplePopup(new PopupRequest
                {
                    Title = AuthTXTHandler.ErrorTitle,
                    Body = AuthTXTHandler.InvalidInput
                });
                return;
            }
            CBSProfile.UpdateDisplayName(newName, onComplete =>
            {
                if (!onComplete.IsSuccess)
                {
                    new PopupViewer().ShowFabError(onComplete.Error);
                }
            });
        }

        private void OnUserNameUpdated(CBSUpdateDisplayNameResult result)
        {
            if (result.IsSuccess)
            {
                NicknameLabel.text = result.DisplayName;
                ShowNickname();
            }
        }

        private void OnAccountInfoGetted(CBSGetAccountInfoResult result)
        {
            DisplayUI();
        }

        private void OnGetProfileInfo(CBSGetProfileDataResult result)
        {
            if (result.IsSuccess)
            {
                var dataDictionaty = result.Data;
                String gameAllKey = "game_all";
                String gameWinsKey = "game_wins";
                String gameLosesKey = "game_loses";
                String gameWinrateKey = "game_winrate";
                String gameAll, gameWins, gameLoses, gameWinrate;
                try
                {
                    gameAll = dataDictionaty[gameAllKey].Value;
                    gameWins = dataDictionaty[gameWinsKey].Value;
                    gameLoses = dataDictionaty[gameLosesKey].Value;
                    gameWinrate = dataDictionaty[gameWinrateKey].Value;
                } catch (Exception e)
                {
                    gameAll = "0";
                    gameWins = "0";
                    gameLoses = "0";
                    gameWinrate = "0";
                    CBSProfile.SaveProfileData(gameAllKey, gameAll, OnSaveProfileData);
                    CBSProfile.SaveProfileData(gameWinsKey, gameWins, OnSaveProfileData);
                    CBSProfile.SaveProfileData(gameLosesKey, gameLoses, OnSaveProfileData);
                    CBSProfile.SaveProfileData(gameWinrateKey, gameWinrate, OnSaveProfileData);
                }
                GameAllLabel.text = gameAll;
                WinValueLabel.text = gameWins;
                DisplayUI();
            }
            else
            {
                Debug.Log(result.Error.Message);
            }
        }

        private void OnSaveProfileData(CBSBaseResult result)
        {
            if (result.IsSuccess)
            {
                Debug.Log("Success!");
            }
            else
            {
                Debug.Log(result.Error.Message);
            }
        }
    }
}
