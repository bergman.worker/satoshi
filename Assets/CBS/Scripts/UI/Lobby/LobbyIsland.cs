﻿using CBS.Scriptable;
using DG.Tweening;
using Satoshi.Animation;
using UnityEngine;
using UnityEngine.UI;


namespace CBS.UI
{
    public class LobbyIsland : MonoBehaviour
    {
        [SerializeField]
        private WindowID Window;
        [SerializeField]
        private Image back;

        public void ClickHandler()
        {
            GameObject windowPrefab = null;
            switch (Window)
            {
                case WindowID.CLAN:
                    {
                        windowPrefab = CBSScriptable.Get<ClanPrefabs>().WindowLoader;
                        break;
                    }
                case WindowID.EVENTS:
                    {
                        windowPrefab = CBSScriptable.Get<EventsPrefabs>().EventWindow;
                        break;
                    }
                case WindowID.FORGE:
                    {
                        windowPrefab = CBSScriptable.Get<CraftPrefabs>().CraftWindow;
                        break;
                    }
                case WindowID.FRIENDS:
                    {
                        windowPrefab = CBSScriptable.Get<FriendsPrefabs>().FriendsWindow;
                        break;
                    }
                case WindowID.MATCHMAKING:
                    {
                        windowPrefab = CBSScriptable.Get<MatchmakingPrefabs>().MatchmalingWindow;
                        break;
                    }
                case WindowID.SHOP:
                    {
                        windowPrefab = CBSScriptable.Get<StorePrefabs>().StoreWindows;
                        break;
                    }
                default:
                    {
                        return;
                    }
            }
               // Отображение окна с анимацией масштабирования
            GameObject windowObject = UIView.ShowWindow(windowPrefab);
            if (windowObject != null)
            {
                // Применить анимацию масштабирования
                // windowObject.transform.localScale = Vector3.one;
                // windowObject.transform.localScale = new Vector3(0, -4000, 0);
                // windowObject.transform.DOLocalMove(Vector3.zero, AnimationSettingsConfigurator.instance.openTime).SetEase(AnimationSettingsConfigurator.instance.easeIn);

                AnimationSettingsConfigurator.instance.InAnimationWithFade(windowObject.transform, back);

                // windowObject.transform.DOScale(1, 0.3f);
            }
            //UIView.ShowWindow(windowPrefab);
           
    
        }
    }
}
