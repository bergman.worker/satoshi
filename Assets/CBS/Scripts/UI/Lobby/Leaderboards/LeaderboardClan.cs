﻿using CBS.Core;
using CBS.Models;
using UnityEngine;
using UnityEngine.UI;
using TMPro; // Добавьте пространство имен для TextMeshPro

namespace CBS.UI
{
    public class LeaderboardClan : MonoBehaviour, IScrollableItem<ClanLeaderboardEntry>
    {
        [SerializeField]
        private Image Background;
        [SerializeField]
        private TMP_Text DisplayName; // Замените на TMP_Text
        [SerializeField]
        private PlaceDrawer Place;
        [SerializeField]
        private ClanAvatarDrawer Avatar;
        [SerializeField]
        private TMP_Text Value; // Замените на TMP_Text
        [Header("Colors")]
        [SerializeField]
        private Color DefaultColor;
        [SerializeField]
        private Color ActiveColor;

        public void Display(ClanLeaderboardEntry data)
        {
            DisplayName.text = data.DisplayName;
            Place.Draw(data.StatisticPosition);
            Value.text = data.StatisticValue.ToString();
            var clanId = data.ClanID;
            var avatarInfo = data.Avatar;

            bool isMine = CBSModule.Get<CBSProfileModule>().ClanID == clanId;
            DisplayName.fontStyle = isMine ? FontStyles.Bold : FontStyles.Normal;
            Value.fontStyle = isMine ? FontStyles.Bold : FontStyles.Normal;
            Background.color = isMine ? ActiveColor : DefaultColor;

            Avatar.Load(clanId, avatarInfo);
        }
    }
}
