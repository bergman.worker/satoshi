﻿using CBS.Models;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro; // Добавьте пространство имен для TextMeshPro

namespace CBS.UI
{
    public class LeaderboardTab : MonoBehaviour
    {
        public LeaderboardTabType TabType;
        public LeaderboardView TabView;
        public string StatisticName;
        public string TitleText;
        public int MaxCount;

        public Action<LeaderboardTab> OnTabSelected;

        public bool IsActive => Toggle.isOn;

        private Toggle toggle;
        private Toggle Toggle
        {
            get
            {
                if (toggle == null)
                {
                    toggle = gameObject.GetComponent<Toggle>();
                }
                return toggle;
            }
        }

        private TMP_Text TitleLabel { get; set; } // Замените на TMP_Text

        private void Awake()
        {
            TitleLabel = GetComponentInChildren<TMP_Text>(); // Замените на TMP_Text
        }

        private void Start()
        {
            Toggle.onValueChanged.AddListener(OnToggleChanged);
            DrawTab();
        }

        private void OnDestroy()
        {
            Toggle.onValueChanged.RemoveListener(OnToggleChanged);
        }

        private void DrawTab()
        {
            TitleLabel.text = TitleText; // Измените на TMP_Text
        }

        // events
        private void OnToggleChanged(bool val)
        {
            if (val)
            {
                OnTabSelected?.Invoke(this);
            }
        }
    }
}
