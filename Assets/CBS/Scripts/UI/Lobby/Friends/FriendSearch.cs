using System.Collections;
using System.Collections.Generic;
using CBS;
using CBS.Models;
using CBS.Scriptable;
using CBS.UI;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class FriendSearch : MonoBehaviour
{
    [SerializeField]
    protected FriendsListScroller Scroller;
    [SerializeField]
    //protected InputField termInput;
    protected TMP_InputField termInput;
    protected IFriends Friends { get; set; }
    protected IProfile Profile { get; set; }
    private FriendsPrefabs prefabs { get; set; }

    private PopupViewer popupViewer;
    protected FriendsPrefabs Prefabs
    {
        get
        {
            prefabs = prefabs ?? CBSScriptable.Get<FriendsPrefabs>();
            return prefabs;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Friends = CBSModule.Get<CBSFriendsModule>();
        Profile = CBSModule.Get<CBSProfileModule>();
    }

    public void Search()
    {
        var term = termInput.text;
        if (term.Length == 0)
        {
            new PopupViewer().ShowSimplePopup(new PopupRequest
            {
                Title = "Error",
                Body = "Search text must be not empty"
            });
            return;
        }
        popupViewer = new PopupViewer();
        popupViewer.ShowLoadingPopup();
        Profile.GetProfileDetailByDisplayName(term, new CBSProfileConstraints
        {
            LoadAvatar = true,
            LoadClan = true,
            LoadLevel = true,
            LoadOnlineStatus = true,
            LoadProfileData = true,
            LoadStatistics = true
        }, OnGetProfileInfo);
    }

    private void OnGetProfileInfo(CBSGetProfileResult result)
    {
        popupViewer.HideLoadingPopup();
        if (result.IsSuccess)
        {
            var profileID = result.ProfileID;
            var clanID = result.ClanID;
            var displayName = result.DisplayName;
            var levelInfo = result.Level;
            var onlineStatus = result.OnlineStatus;
            var clanEntity = result.ClanEntity;
            var statistics = result.Statistics;
            var profileData = result.ProfileData;
            var avatar = result.Avatar;
            var uiPrefab = Prefabs.FoundFriendsUI;

            ProfileEntity profileEntity = new ProfileEntity
            {
                ProfileID = profileID,
                DisplayName = displayName,
                Avatar = avatar,
                ClanID = clanID,
                Level = levelInfo,
                Statistics = statistics,
                ProfileData = profileData,
                OnlineStatus = onlineStatus,
                ClanEntity = clanEntity
            };

            var list = new List<ProfileEntity>();
            list.Add(profileEntity);
            Scroller.Spawn(uiPrefab, list);
        }
        else
        {
            new PopupViewer().ShowSimplePopup(new PopupRequest
            {
                Title = "Error",
                Body = result.Error.Message
            });
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
