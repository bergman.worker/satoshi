﻿using CBS.Models;

namespace CBS.UI
{
    public class FoundFriendUI : FriendUI
    {
        private PopupViewer popupViewer;
        public void InviteFriend()
        {
            string userID = CurrentFriend.ProfileID;
            popupViewer = new PopupViewer();
            popupViewer.ShowLoadingPopup();
            Friends.SendFriendsRequest(userID, OnSent);
        }

        private void OnSent(CBSSendFriendsRequestResult result)
        {
            popupViewer.HideLoadingPopup();
            if (result.IsSuccess)
            {
                new PopupViewer().ShowSimplePopup(new PopupRequest
                {
                    Title = "Success",
                    Body = "Friend request has been sent"
                });
            }
            else
            {
                new PopupViewer().ShowSimplePopup(new PopupRequest
                {
                    Title = "Error",
                    Body = result.Error.Message
                });
            }
        }
    }
}
