using System;
using DG.Tweening;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Satoshi.Animation
{
    [CreateAssetMenu(menuName = "Satoshi/AnimationSettingsConfigurator")]
    public class AnimationSettingsConfigurator : ScriptableObject
    {
        private static AnimationSettingsConfigurator _instance;

        public static AnimationSettingsConfigurator instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = Resources.Load<AnimationSettingsConfigurator>("AnimationSettingsConfigurator");

                    if (_instance == null)
                    {
                        Debug.LogError("AnimationSettingsConfigurator has not been created. Create it via the Create Asset menu.");
                    }
                }
                return _instance;
            }
        }

        // public static AnimationSettingsConfigurator instance;
        // private void OnValidate()
        // {
        //     instance = this;
        //     Debug.Log("Hello world");
        // }

        Image backGround;

        [SerializeField] private AnimationType animationType;

        public void InAnimation(Transform target)
        {
            target.gameObject.SetActive(true);

            switch (animationType)
            {
                default:
                    break;
                case AnimationType.Move:
                    MoveUpAnimation(target);
                    break;
                case AnimationType.Scale:
                    ExpandAnimation(target);
                    break;
            }

            if (backGround != null)
            {
                backGround.raycastTarget = true;
                backGround.DOFade(0.6f, openTime);
            }
        }

        public void InAnimationWithFade(Transform target, Image bg)
        {
            bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, 0);
            bg.raycastTarget = true;

            switch (animationType)
            {
                default:
                    break;
                case AnimationType.Move:
                    MoveUpAnimation(target);
                    break;
                case AnimationType.Scale:
                    ExpandAnimation(target);
                    break;
            }
            bg.DOFade(0.6f, openTime);
            backGround = bg;
        }

        public void OutAnimation(Transform target)
        {
            switch (animationType)
            {
                default:
                    break;
                case AnimationType.Move:
                    MoveDownAnimation(target);
                    break;
                case AnimationType.Scale:
                    ShrinkAnimation(target);
                    break;
            }

            if (backGround != null)
            {
                backGround.DOFade(0, closeTime).OnComplete(() => backGround.raycastTarget = false);
            }
        }

        public void OutAnimationWithFade(Transform target, Image bg)
        {
            switch (animationType)
            {
                default:
                    break;
                case AnimationType.Move:
                    MoveDownAnimation(target);
                    break;
                case AnimationType.Scale:
                    ShrinkAnimation(target);
                    break;
            }

            bg.DOFade(0, closeTime);
        }

        public void ExpandAnimation(Transform target, Action onComplete = null)
        {
            target.localPosition = Vector3.zero;
            target.localScale = Vector3.zero;
            target.DOScale(1, openTime).SetEase(easeIn).OnComplete(() => onComplete?.Invoke());
        }

        public void ShrinkAnimation(Transform target)
        {
            target.DOScale(0, closeTime).OnComplete(() =>
    {
        target.gameObject.SetActive(false);
        target.DOKill();
    }).SetEase(easeOut);
        }

        public void MoveUpAnimation(Transform target, Action onComplete = null)
        {
            target.localScale = Vector3.one;
            target.localPosition = new Vector3(0, -4000, 0);
            target.DOLocalMove(Vector3.zero, openTime).SetEase(easeIn).OnComplete(() => onComplete?.Invoke());
        }

        public void MoveDownAnimation(Transform target)
        {
            target.DOLocalMoveY(-4000, closeTime).OnComplete(() =>
    {
        target.gameObject.SetActive(false);
        target.DOKill();
    }).SetEase(easeOut);
        }


        [SerializeField] private Ease _easeIn;

        public Ease easeIn { get; set; }

        public Ease easeOut;

        public float openTime;
        public float closeTime;
    }

    public enum AnimationType
    {
        Move,
        Scale
    }
}