﻿
namespace CBS.Models
{
    public class FunctionUpdateDisplayNameRequest : FunctionBaseRequest
    {
        public string DisplayName;
    }
}
