﻿using CBS.Models;
using CBS.Playfab;
using CBS.Utils;
using PlayFab;
using PlayFab.CloudScriptModels;
using PlayFab.MultiplayerModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace CBS
{
    public class CBSMatchmakingModule : CBSModule, IMatchmaking
    {
        /// <summary>
        /// Notifies about status change in Matchmaking
        /// </summary>
        public event Action<CBSCheckPVPResult> OnStatusChanged;
        /// <summary>
        /// Notifies about the successful completion of the search for opponents.
        /// </summary>
        public event Action<CBSStartMatchResult> OnMatchStart;

        private IFabMatchmaking FabMatchmaking { get; set; }
        private IProfile Profile { get; set; }

        /// <summary>
        /// Current Queue name
        /// </summary>
        public string ActiveQueue { get; private set; }
        /// <summary>
        /// Current ticket id name
        /// </summary>
        public string ActiveTicketID { get; private set; }
        /// <summary>
        /// Active matchmaking status
        /// </summary>
        public CBSCheckPVPResult MatchInfo { get; private set; }

        private bool CompareIsRunning { get; set; }
        private Coroutine CompareCoroutine { get; set; }

        protected override void Init()
        {
            FabMatchmaking = FabExecuter.Get<FabMatchmaking>();
            Profile = Get<CBSProfileModule>();
            StartRefreshTask();
        }

        // internal
        private void StartRefreshTask()
        {
            CompareIsRunning = true;
            CompareWorkTask();
        }

        private void StopRefreshTask()
        {
            CompareIsRunning = false;
            if (CompareCoroutine != null)
            {
                CoroutineRunner.StopCoroutine(CompareCoroutine);
            }
        }

        private void CompareWorkTask()
        {
            CompareCoroutine = CoroutineRunner.StartCoroutine(CompareWorkTaskCoroutine());
        }

        private IEnumerator CompareWorkTaskCoroutine()
        {
            yield return new WaitForSeconds(CBSConstants.MatchmakingRefreshTime);

            if (!CompareIsRunning)
            {
                yield break;
            }

            Debug.Log("test");

            FabMatchmaking.CheckPVP(Profile.ProfileID, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    CompareWorkTask();
                }
                else
                {
                    var checkPvpResult = onGet.GetResult<FunctionCheckPVPResult>();
                    var matchInfo = new CBSCheckPVPResult
                    {
                        IsSuccess = true,
                        pvpId = checkPvpResult.pvpId,
                        CreatorId = checkPvpResult.CreatorId,
                        OpponentId = checkPvpResult.OpponentId,
                        CreatorName = checkPvpResult.CreatorName,
                        OpponentName = checkPvpResult.OpponentName,
                        bet = checkPvpResult.bet,
                        Status = checkPvpResult.Status
                    };
                    SetMatchInfo(matchInfo);
                    CompareWorkTask();
                }
            }, onFailed =>
            {
                CompareWorkTask();
            });
        }

        private void SetMatchInfo(CBSCheckPVPResult matchInfo)
        {
           if (MatchInfo == null || MatchInfo.pvpId != matchInfo.pvpId)
           {
                MatchInfo = matchInfo;
                OnStatusChanged?.Invoke(matchInfo);
           }
           MatchInfo = matchInfo;
        }


        protected override void OnLogout()
        {
            base.OnLogout();
            ClearInternalProcess();
        }

        private void ClearInternalProcess()
        {
            ActiveQueue = string.Empty;
            ActiveTicketID = string.Empty;

            StopRefreshTask();

            SetMatchInfo(null);
        }

        public void GetRewardsFromVersusBattle(string profileID, bool isWinner, int bet, Action<CBSGetRewardFromVersusBattleResult> result)
        {
            FabMatchmaking.GetRewardsFromVersusBattle(profileID, isWinner, bet, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSGetRewardFromVersusBattleResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var fabResult = onGet.GetResult<FunctionGetRewardFromVersusBattleResult>();

                    result?.Invoke(new CBSGetRewardFromVersusBattleResult
                    {
                        IsSuccess = true,
                        amount = fabResult.amount,
                        exp = fabResult.exp
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSGetRewardFromVersusBattleResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void CreatePVP(string ProfileId, string OpponentId, int bet, Action<CBSCheckPVPResult> result)
        {
            FabMatchmaking.CreatePVP(ProfileId, OpponentId, bet, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSCheckPVPResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var checkPvpResult = onGet.GetResult<FunctionCheckPVPResult>();

                    result?.Invoke(new CBSCheckPVPResult
                    {
                        IsSuccess = true,
                        pvpId = checkPvpResult.pvpId,
                        CreatorId = checkPvpResult.CreatorId,
                        OpponentId = checkPvpResult.OpponentId,
                        CreatorName = checkPvpResult.CreatorName,
                        OpponentName = checkPvpResult.OpponentName,
                        bet = checkPvpResult.bet,
                        Status = checkPvpResult.Status
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSCheckPVPResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void CheckPVP(string ProfileId, Action<CBSCheckPVPResult> result)
        {
            FabMatchmaking.CheckPVP(ProfileId, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSCheckPVPResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var checkPvpResult = onGet.GetResult<FunctionCheckPVPResult>();

                    result?.Invoke(new CBSCheckPVPResult
                    {
                        IsSuccess = true,
                        pvpId = checkPvpResult.pvpId,
                        CreatorId = checkPvpResult.CreatorId,
                        OpponentId = checkPvpResult.OpponentId,
                        CreatorName = checkPvpResult.CreatorName,
                        OpponentName = checkPvpResult.OpponentName,
                        bet = checkPvpResult.bet,
                        Status = checkPvpResult.Status
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSCheckPVPResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void AcceptPVP(string pvpId, Action<CBSBaseResult> result)
        {
            FabMatchmaking.AcceptPVP(pvpId, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var fabResult = onGet.GetResult<ExecuteFunctionResult>();

                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = true
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSBaseResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void DeclinePVP(string pvpId, Action<CBSBaseResult> result)
        {
            FabMatchmaking.DeclinePVP(pvpId, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var fabResult = onGet.GetResult<ExecuteFunctionResult>();

                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = true
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSBaseResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void CancelPVP(string pvpId, Action<CBSBaseResult> result)
        {
            FabMatchmaking.CancelPVP(pvpId, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var fabResult = onGet.GetResult<ExecuteFunctionResult>();

                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = true
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSBaseResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void StartPVP(string pvpId, Action<CBSBaseResult> result)
        {
            FabMatchmaking.StartPVP(pvpId, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var fabResult = onGet.GetResult<ExecuteFunctionResult>();

                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = true
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSBaseResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void StartVersusBattle(string ProfileId, int bet, Action<CBSBaseResult> result)
        {
            FabMatchmaking.StartVersusBattle(ProfileId, bet, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var fabResult = onGet.GetResult<ExecuteFunctionResult>();

                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = true
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSBaseResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void StartCasinoBattle(string ProfileId, int bet, Action<CBSBaseResult> result)
        {
            FabMatchmaking.StartCasinoBattle(ProfileId, bet, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var fabResult = onGet.GetResult<ExecuteFunctionResult>();

                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = true
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSBaseResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void GetRewardsFromCasinoBattle(string profileID, bool isWinner, int bet, Action<CBSGetRewardFromVersusBattleResult> result)
        {
            FabMatchmaking.GetRewardsFromCasinoBattle(profileID, isWinner, bet, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSGetRewardFromVersusBattleResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var fabResult = onGet.GetResult<FunctionGetRewardFromVersusBattleResult>();

                    result?.Invoke(new CBSGetRewardFromVersusBattleResult
                    {
                        IsSuccess = true,
                        amount = fabResult.amount,
                        exp = fabResult.exp
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSGetRewardFromVersusBattleResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }

        public void FinishPVP(string pvpId, string winnerId, Action<CBSBaseResult> result)
        {
            FabMatchmaking.FinishPVP(pvpId, winnerId, onGet =>
            {
                var cbsError = onGet.GetCBSError();
                if (cbsError != null)
                {
                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = false,
                        Error = cbsError
                    });
                }
                else
                {
                    var fabResult = onGet.GetResult<ExecuteFunctionResult>();

                    result?.Invoke(new CBSBaseResult
                    {
                        IsSuccess = true
                    });
                }
            }, onFailed =>
            {
                result?.Invoke(new CBSBaseResult
                {
                    IsSuccess = false,
                    Error = CBSError.FromTemplate(onFailed)
                });
            });
        }
    }
}
