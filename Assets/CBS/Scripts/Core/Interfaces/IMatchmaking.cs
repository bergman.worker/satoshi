﻿using CBS.Models;
using System;

namespace CBS
{
    public interface IMatchmaking
    {
        /// <summary>
        /// Notifies about status change in Matchmaking
        /// </summary>
        event Action<CBSCheckPVPResult> OnStatusChanged;

        /// <summary>
        /// Notifies about the successful completion of the search for opponents.
        /// </summary>
        event Action<CBSStartMatchResult> OnMatchStart;

        /// <summary>
        /// Current Queue name
        /// </summary>
        string ActiveQueue { get; }

        /// <summary>
        /// Current ticket id name
        /// </summary>
        string ActiveTicketID { get; }

        /// <summary>
        /// Active matchmaking status
        /// </summary>
        CBSCheckPVPResult MatchInfo { get; }



        void CreatePVP(string ProfileId, string OpponentId, int bet, Action<CBSCheckPVPResult> result);

        void CheckPVP(string ProfileId, Action<CBSCheckPVPResult> result);

        void AcceptPVP(string pvpId, Action<CBSBaseResult> result);

        void DeclinePVP(string pvpId, Action<CBSBaseResult> result);

        void CancelPVP(string pvpId, Action<CBSBaseResult> result);

        void StartPVP(string pvpId, Action<CBSBaseResult> result);

        void FinishPVP(string pvpId, string winnerId, Action<CBSBaseResult> result);

        void StartVersusBattle(string ProfileId, int bet, Action<CBSBaseResult> result);

        void StartCasinoBattle(string ProfileId, int bet, Action<CBSBaseResult> result);

        void GetRewardsFromVersusBattle(string profileID, bool isWinner, int bet, Action<CBSGetRewardFromVersusBattleResult> result);
        void GetRewardsFromCasinoBattle(string profileID, bool isWinner, int bet, Action<CBSGetRewardFromVersusBattleResult> result);
    }
}
