using System;
using System.Collections;
using System.Net.Http;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class LoadAssets : MonoBehaviour
{
    private const string VersionKey = "Ver";
    private const string LoginSceneKey = "Assets/Scenes/Login.unity";
    public TextMeshProUGUI progressBar;

    [SerializeField] private Slider slider;
    [SerializeField] private TextMeshProUGUI progText;

    async void Start()
    {
        var curVers = PlayerPrefs.GetString(VersionKey);

        //todo Если сделать фетч с Version.json
        var v = new Version();

        v = JsonUtility.FromJson<Version>(await GetJsonStringAsync("https://storage.yandexcloud.net/sfc-bucket/Android/Version.json"));
        Debug.Log(v.currentVersion);

        if (string.IsNullOrEmpty(curVers))
        {
            Debug.Log("First Launch");
            PlayerPrefs.SetString(VersionKey, v.currentVersion);
        }

        // var go = Addressables.LoadContentCatalog

        StartCoroutine(LoadScene(LoginSceneKey));

        // if (scene.Status == UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus.Failed)
        // {
        //     Debug.Log("Some fail while loading resources " + scene.GetDownloadStatus());
        // }
    }

    [ContextMenu("Clear Cache")]
    public void ClearCache()
    {
        Addressables.ClearDependencyCacheAsync(Addressables.ResourceLocators, true);
    }

    public IEnumerator LoadScene(string sceneName)
    {
        AsyncOperationHandle handle = Addressables.DownloadDependenciesAsync("Assets/Scenes/Lobby.unity");
        StartCoroutine(ShowProgress(handle));
        yield return handle;
        Addressables.LoadSceneAsync(sceneName);
    }

    // private IEnumerator DownloadAll()
    // {
    //     // AsyncOperationHandle downloadDependencies = Addressables.DownloadDependenciesAsync(key);
    // }

    private IEnumerator ShowProgress(AsyncOperationHandle operation)
    {
        while (operation.PercentComplete < 1 && !operation.IsDone)
        {
            slider.value = operation.PercentComplete;

            progText.text = operation.PercentComplete * 100 + "%";

            // Debug.Log(operation.PercentComplete);
            //    progressBar.text = "Downloading Asset: " + operation.PercentComplete.ToString();
            yield return null;
        }
    }

    public async Task<string> GetJsonStringAsync(string url)
    {
        HttpClient client = new HttpClient();
        HttpResponseMessage response = await client.GetAsync(url);
        if (response.IsSuccessStatusCode)
        {
            string jsonString = await response.Content.ReadAsStringAsync();
            return jsonString;
        }
        else
        {
            return null;
        }
    }
}

[Serializable]
public struct Version
{
    public string currentVersion;
}
